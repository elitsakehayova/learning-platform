import React from 'react';
import { Route } from 'react-router-dom';
import Courses from './Courses';
import Dashboard from './Dashboard';

export default [
  {
    exact: true,
    path: '/my-courses',
    component: Courses,
  },
  {
    exact: true,
    path: '/my-dashboard',
    component: Dashboard,
  },
].map(route => <Route key={route.path} {...route} />);

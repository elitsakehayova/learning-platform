import React from 'react';
import { Route } from 'react-router-dom';
import Landing from './Landing';
import Login from './Login';

export default [
  {
    exact: true,
    path: '/',
    component: Landing,
  },
  {
    exact: true,
    path: '/login',
    component: Login,
  },
].map(route => <Route key={route.path} {...route} />);

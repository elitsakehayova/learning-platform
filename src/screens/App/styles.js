import { css } from 'emotion';

export const applicationContainer = css({
  width: '100vw',
  height: '100vh',
  paddingTop: 60,
});

export const contentContainer = css({
  width: '100%',
  minHeight: '100%',
  position: 'relative',
  display: 'flex',
});

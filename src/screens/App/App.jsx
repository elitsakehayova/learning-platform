import React from 'react';
import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';
import PrivateRoutes from '../Private/Routes';
import PublicRoutes from '../Public/Routes';

const isAuthenticated = false;

const App = () => (
  <Router>
    <Switch>
      {isAuthenticated ? PrivateRoutes : PublicRoutes}
      <Redirect to={isAuthenticated ? '/my-dashboard' : '/'} />
    </Switch>
  </Router>
);

export default App;

import React from 'react';
import { render } from 'react-dom';
import { hot } from 'react-hot-loader/root';
import BaseApp from './screens/App/App';
import { Theme } from './components/UI';
import initGlobalStyles from './assets/css/globalCSS';

const AuthenticatedApp = () => (
  <Theme initGlobalStyles={initGlobalStyles}>
    <BaseApp />
  </Theme>
);

const App = env === 'development' ? hot(AuthenticatedApp) : AuthenticatedApp;

render(<App />, document.getElementById('learning-platform-root'));

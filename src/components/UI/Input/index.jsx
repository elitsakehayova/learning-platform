import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { cx } from 'emotion';
import { withTheme } from 'emotion-theming';
import Datetime from 'react-datetime';
import moment from 'moment';
import { isObject, isUndefined, omit } from 'lodash';
import { inputField } from './styles';

const Input = forwardRef(
  ({ value, onChange, theme, dateTimeProps, className, ...inputAttr }, ref) => {
    const inputProps =
      isUndefined(value) || isUndefined(onChange)
        ? { ref }
        : { value: value || '', onChange, ref };

    return dateTimeProps ? (
      <Datetime
        dateFormat={dateTimeProps.format ?? 'DD MMM YYYY'}
        onBlur={inputAttr.onBlur}
        value={
          value && moment(value).format(dateTimeProps.format ?? 'DD MMM YYYY')
        }
        closeOnSelect
        onChange={val => isObject(val) && onChange({ target: { value: val } })}
        inputProps={{
          ref,
          className: inputField(theme),
        }}
        {...omit(dateTimeProps, 'format')}
      />
    ) : (
      <input
        ref={ref}
        className={cx(inputField(theme), className)}
        {...inputProps}
        {...inputAttr}
      />
    );
  }
);

Input.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  dateTimeProps: PropTypes.object,
  theme: PropTypes.object,
};

export default withTheme(Input);

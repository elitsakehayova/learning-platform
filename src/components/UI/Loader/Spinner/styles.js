import { css, keyframes } from 'emotion';

const loaderAnimation = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

export const loader = (size, { color, fullScreen }, theme) =>
  css(
    {
      width: size,
      height: size,
      position: 'absolute',
      top: `calc(50% - ${size / 2}px)`,
      left: `calc(50% - ${size / 2}px)`,
      zIndex: 10,
      borderRadius: '50%',
      borderRight: '4px solid transparent',
      borderTop: `${size > 48 ? 4 : 3}px solid ${theme[color] ||
        theme.primary}`,
      animation: `${
        size > 48 ? `${loaderAnimation} 2s` : `1s ${loaderAnimation}`
      } linear infinite`,

      '&:before, &:after': {
        content: size > 48 ? '""' : 'none',
        position: 'absolute',
        borderRadius: '50%',
        borderRight: '4px solid transparent',
        borderTop: `4px solid ${theme[color] || theme.primary}`,
      },

      '&:before': {
        top: 5,
        left: 5,
        right: 5,
        bottom: 5,
        animation: `${loaderAnimation} 3.5s linear infinite`,
      },

      '&:after': {
        top: 15,
        left: 15,
        right: 15,
        bottom: 15,
        animation: `${loaderAnimation} 1.75s linear infinite`,
      },
    },
    fullScreen && {
      position: 'fixed',
    }
  );

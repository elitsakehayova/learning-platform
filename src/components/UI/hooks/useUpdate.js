import { useEffect } from 'react';
import hash from 'object-hash';
import { useConstant } from './useConstant';

export const useUpdate = (method, deps) => {
  const [isMounted, setIsMounted] = useConstant(false);

  useEffect(() => {
    isMounted && method();
    !isMounted && setIsMounted(true);
  }, [hash({ deps })]);
};

export const useUpdateOnce = (method, deps) => {
  const [isMounted, setIsMounted] = useConstant(false);

  useEffect(() => {
    if (isMounted) {
      method();
      setIsMounted(false);
    }
    !isMounted && setIsMounted(true);
  }, [hash({ deps })]);
};

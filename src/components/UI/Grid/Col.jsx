import React from 'react';
import PropTypes from 'prop-types';
import { fromPairs } from 'lodash';
import { colComp } from './styles';

const Col = props => {
  const {
    children,
    role,
    tabIndex,
    onClick,
    onMouseDown,
    onMouseUp,
    onMouseEnter,
    onMouseLeave,
    onDrag,
  } = props;
  let eventProps = {};
  const events = [
    ['onClick', onClick],
    ['onMouseDown', onMouseDown],
    ['onMouseUp', onMouseUp],
    ['onMouseEnter', onMouseEnter],
    ['onMouseLeave', onMouseLeave],
    ['onDrag', onDrag],
  ].filter(el => Boolean(el[1]));

  if (events.length) {
    events.push(['role', role || 'button'], ['tabIndex', tabIndex || -1]);
    eventProps = fromPairs(events);
  }

  return (
    <div className={colComp(props)} {...eventProps}>
      {children}
    </div>
  );
};

Col.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.string,
  ]),
  role: PropTypes.string,
  tabIndex: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  onMouseDown: PropTypes.func,
  onMouseUp: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  onDrag: PropTypes.func,
};

export default Col;

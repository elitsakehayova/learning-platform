import { css } from 'emotion';

export const row = {
  width: '100%',
  display: 'flex',
  flexWrap: 'wrap',
  flexDirection: 'row',
};

export const rowComp = (
  {
    column,
    reverse,
    noWrap,
    align,
    justify,
    padding,
    margin,
    minWidth,
    className,
  },
  minWidthDefault
) =>
  css(
    row,
    {
      flexDirection: column
        ? reverse
          ? 'column-reverse'
          : 'column'
        : reverse
        ? 'row-reverse'
        : 'row',
      flexWrap: noWrap ? 'no-wrap' : 'wrap',
      padding: padding || 0,
      margin: margin || 0,
      minWidth: minWidth || minWidthDefault,
    },
    align && {
      alignItems: align,
    },
    justify && {
      justifyContent: justify,
    },
    className || {}
  );

const baseCol = {
  flexBasis: '100%',
  maxWidth: '100%',
  flexGrow: 1,
  flexShrink: 0,
  outline: 'none',
};

export const col = baseCol;

export const xsCol = xs =>
  xs && {
    ...baseCol,
    '@media (min-width: 576px)': {
      flexBasis: `${100 / xs}%`,
      maxWidth: `${100 / xs}%`,
    },
  };

export const smCol = sm =>
  sm && {
    ...baseCol,
    '@media (min-width: 768px)': {
      flexBasis: `${100 / sm}%`,
      maxWidth: `${100 / sm}%`,
    },
  };

export const mdCol = md =>
  md && {
    ...baseCol,
    '@media (min-width: 992px)': {
      flexBasis: `${100 / md}%`,
      maxWidth: `${100 / md}%`,
    },
  };

export const lgCol = lg =>
  lg && {
    ...baseCol,
    '@media (min-width: 1200px)': {
      flexBasis: `${100 / lg}%`,
      maxWidth: `${100 / lg}%`,
    },
  };

export const xlCol = xl =>
  xl && {
    ...baseCol,
    '@media (min-width: 1440px)': {
      flexBasis: `${100 / xl}%`,
      maxWidth: `${100 / xl}%`,
    },
  };

export const colComp = ({
  xs,
  sm,
  md,
  lg,
  xl,
  className = {},
  gap,
  verticalGap,
  horizontalGap,
}) =>
  css(
    baseCol,
    xsCol(xs),
    smCol(sm),
    mdCol(md),
    lgCol(lg),
    xlCol(xl),
    gap && {
      padding: gap,
    },
    verticalGap && {
      paddingTop: verticalGap,
      paddingBottom: verticalGap,
    },
    horizontalGap && {
      paddingLeft: horizontalGap,
      paddingRight: horizontalGap,
    },
    className
  );

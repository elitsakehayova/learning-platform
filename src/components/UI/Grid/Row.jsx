import React, { cloneElement, Children } from 'react';
import PropTypes from 'prop-types';
import { inRange, isPlainObject } from 'lodash';
import { rowComp } from './styles';

let NUMBER_OF_COLUMNS = 12;
let MIN_WIDTH = 0;

export const setUpGrid = options => {
  if (!isPlainObject(options)) return;

  NUMBER_OF_COLUMNS = options.numberOfColumns || 12;
  MIN_WIDTH = options.minWidth || 0;
};

const Row = props => {
  const { toArray } = Children;
  const { children: initChilds, xs, sm, md, lg, xl } = props;
  const { gap, verticalGap, horizontalGap } = props;
  const children = toArray(initChilds).filter(Boolean);

  if (
    [xs, sm, md, lg, xl]
      .filter(Boolean)
      .some(el => el < 1 || el > NUMBER_OF_COLUMNS)
  )
    return null;

  const getSize = (el, type) => {
    const size = el.props[type] || 0;
    return inRange(size, 1, 13) ? NUMBER_OF_COLUMNS / size : null;
  };

  const columnChildrens = children.map((el, i) =>
    cloneElement(el, {
      key: `Column${i}`,
      gap,
      verticalGap,
      horizontalGap,
      xs: getSize(el, 'xs') || (xs ? NUMBER_OF_COLUMNS / xs : null),
      sm: getSize(el, 'sm') || (sm ? NUMBER_OF_COLUMNS / sm : null),
      md: getSize(el, 'md') || (md ? NUMBER_OF_COLUMNS / md : null),
      lg: getSize(el, 'lg') || (lg ? NUMBER_OF_COLUMNS / lg : null),
      xl: getSize(el, 'xl') || (xl ? NUMBER_OF_COLUMNS / xl : null),
    })
  );

  return <div className={rowComp(props, MIN_WIDTH)}>{columnChildrens}</div>;
};

Row.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xl: PropTypes.number,
  gap: PropTypes.number,
  verticalGap: PropTypes.number,
  horizontalGap: PropTypes.number,
};

export default Row;

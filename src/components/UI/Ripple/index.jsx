import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { cx } from 'emotion';
import { withTheme } from 'emotion-theming';
import { rippleBaseStyles, animationOn, onFocus } from './styles';

const Ripple = props => {
  const rippleClassName = useRef();
  const onFocusClassName = useRef();

  const {
    children,
    className,
    backColor,
    focusColor,
    flat,
    guideId,
    theme,
  } = props;
  const guideIdProp = guideId ? { 'data-guide-id': guideId } : {};

  const handleMouseDown = e => {
    e.preventDefault();
    const {
      width,
      height,
      left,
      top,
    } = e.currentTarget.getBoundingClientRect();
    const size = Math.max(width, height);

    const x = flat ? 0 : parseInt(e.clientX - left - size / 2, 10);
    const y = flat ? 0 : parseInt(e.clientY - top - size / 2, 10);
    rippleClassName.current = animationOn(x, y, size);
    e.currentTarget.classList.add(rippleClassName.current);
    e.currentTarget.focus();
  };

  const handleMouseUp = e => {
    e.preventDefault();
    const elementClass = e.currentTarget.classList;
    setTimeout(() => {
      elementClass.remove(rippleClassName.current);
    }, 180);
  };

  const handleFocus = e => {
    e.preventDefault();
    onFocusClassName.current = onFocus(theme, focusColor);
    e.currentTarget.classList.add(onFocusClassName.current);
  };

  const handleBlur = e => {
    e.preventDefault();
    e.currentTarget.classList.remove(onFocusClassName.current);
  };

  return (
    <div
      role="presentation"
      onMouseDown={handleMouseDown}
      onMouseUp={handleMouseUp}
      onMouseLeave={handleMouseUp}
      onFocus={handleFocus}
      onBlur={handleBlur}
      className={cx(rippleBaseStyles(theme, backColor, focusColor), className)}
      {...guideIdProp}>
      {children}
    </div>
  );
};

Ripple.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  backColor: PropTypes.string,
  focusColor: PropTypes.string,
  flat: PropTypes.bool,
  guideId: PropTypes.string,
  theme: PropTypes.object,
};

export default withTheme(Ripple);

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { isFunction } from 'lodash';
import { useTheme } from 'emotion-theming';
import Icon from '../Icon';
import { Timer } from '../utils';
import { useConstant } from '../hooks';
import { notification, notificationTitle, notifContainer } from './styles';

const getIcon = {
  success: 'check',
  error: 'error',
  warning: 'warning',
  info: 'info',
};

const Notification = props => {
  const {
    id,
    onRemoval,
    title,
    message,
    content,
    duration = 7000,
    toggleRemoval,
    onClick,
    type,
    iconName,
    alwaysShow = false,
    pauseOnHover = true,
  } = props;
  const [, setTimer, timer] = useConstant();
  const [callback, setCallback] = useConstant(onRemoval);
  const [hide, setHide] = useState(false);
  const theme = useTheme();

  const onTransitionStart = () => {
    if (!duration || alwaysShow) return;
    const notifTimer = new Timer(() => setHide(true), duration);
    setTimer(notifTimer);
    notifTimer.start();
  };

  useEffect(() => {
    pauseOnHover && onTransitionStart();

    return () => {
      pauseOnHover && timer.current?.clear();
    };
  }, []);

  const onTransitionEnd = () => {
    toggleRemoval(id);
    isFunction(callback) && callback(props);
  };

  const onMouseEnter = () => timer.current?.pause();

  const onMouseLeave = () => timer.current?.start();

  const handleClick = () => {
    setCallback(isFunction(onClick) ? onClick : onRemoval);
    setHide(true);
  };

  return (
    <div
      role="presentation"
      className={notification(type, hide, theme)}
      onMouseEnter={pauseOnHover ? onMouseEnter : null}
      onMouseLeave={pauseOnHover ? onMouseLeave : null}
      onClick={handleClick}
      onTransitionEnd={hide ? onTransitionEnd : onTransitionStart}>
      {content ?? (
        <div className={notifContainer}>
          {(iconName || getIcon[type]) && (
            <Icon iconName={iconName ?? getIcon[type]} />
          )}
          <div>
            {title && <p className={notificationTitle}>{title}</p>}
            {message && <p>{message}</p>}
          </div>
        </div>
      )}
    </div>
  );
};

Notification.propTypes = {
  id: PropTypes.string,
  onRemoval: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  content: PropTypes.node,
  duration: PropTypes.number,
  type: PropTypes.string,
  toggleRemoval: PropTypes.func,
  onClick: PropTypes.func,
  alwaysShow: PropTypes.bool,
  iconName: PropTypes.string,
  pauseOnHover: PropTypes.bool,
};

export default Notification;

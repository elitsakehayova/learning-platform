import { isString, isObject, isEmpty, isArray } from 'lodash';
import { uuid } from '../utils';

export const Types = {
  SUCCESS: 'success',
  ERROR: 'error',
  INFO: 'info',
  WARNING: 'warning',
};

const prepare = notif =>
  isString(notif) ? { message: notif, id: uuid() } : { ...notif, id: uuid() };

const handleApiError = ({ data: res }) => {
  if (isString(res)) return [{ message: res }];

  const errors = isObject(res) ? res.errors : null;
  if (!errors || isEmpty(errors)) return [];
  return errors.map(el => {
    const message =
      isArray(el.codes) && el.codes.length
        ? `Code: ${el.codes.join(', ')}`
        : null;
    return { title: el.text, message };
  });
};

class Store {
  queue = [];

  add = () => {};

  remove = () => {};

  show = notif => this.add(prepare(notif));

  showSuccess = notif => this.add({ ...prepare(notif), type: Types.SUCCESS });

  showError = notif => this.add({ ...prepare(notif), type: Types.ERROR });

  showApiError = error => {
    handleApiError(error).forEach(notif =>
      this.add({ ...prepare(notif), type: Types.ERROR })
    );
  };

  showInfo = notif => this.add({ ...prepare(notif), type: Types.INFO });

  showWarning = notif => this.add({ ...prepare(notif), type: Types.WARNING });

  removeNotification = id => {
    const notifId = this.queue.findIndex(el => el.id === id);
    if (notifId !== -1) {
      this.queue.splice(notifId, 1);
      this.remove(this.queue);
    }
  };

  register = ({ addNotification, removeNotification }) => {
    this.add = addNotification;
    this.remove = removeNotification;
  };
}

export default new Store();

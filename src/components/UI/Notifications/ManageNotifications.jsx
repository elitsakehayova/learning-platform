import React, { useEffect, useState } from 'react';
import BodyOverflow from '../BodyOverflow';
import Notification from './Notification';
import store from './Store';
import { notificationsOverlay, allNotificationsContainer } from './styles';

const ManageNotifications = () => {
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    store.register({
      addNotification: add,
      removeNotification: remove,
    });
  }, []);

  const add = notification => {
    setNotifications(prev =>
      notification.insertOnTop
        ? [notification, ...prev]
        : [...prev, notification]
    );

    return notification;
  };

  const remove = id =>
    setNotifications(prev => prev.filter(({ id: nId }) => nId !== id));

  return (
    <BodyOverflow className={notificationsOverlay}>
      <div className={allNotificationsContainer}>
        {notifications.map(notification => (
          <Notification
            key={notification.id}
            {...notification}
            toggleRemoval={remove}
          />
        ))}
      </div>
    </BodyOverflow>
  );
};

export default ManageNotifications;

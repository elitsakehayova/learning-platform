import Store from './Store';

export { default as Notifications } from './ManageNotifications';

export const {
  show,
  showSuccess,
  showError,
  showApiError,
  showInfo,
  showWarning,
  removeNotification,
} = Store;

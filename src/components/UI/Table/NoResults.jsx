import React from 'react';
import { useTheme } from 'emotion-theming';
import Icon from '../Icon';
import {
  noResultsContainer,
  noResultsTitle,
  noResultsIcon,
  noResultsSubtitle,
} from './styles';

export const NoResults = () => {
  const theme = useTheme();

  return (
    <section className={noResultsContainer}>
      <div className={noResultsIcon(theme)}>
        <Icon iconName="search" size={48} />
      </div>
      <h2 className={noResultsTitle}>There are no results</h2>
      <p className={noResultsSubtitle(theme)}>
        For those filters there is no match.
        <br />
        Please try with another search
      </p>
    </section>
  );
};

export const Error = () => {
  const theme = useTheme();

  return (
    <section className={noResultsContainer}>
      <div className={noResultsIcon(theme)}>
        <Icon iconName="report" size={48} />
      </div>
      <h2 className={noResultsTitle}>Request failure.</h2>
      <p className={noResultsSubtitle(theme)}>
        There was problem getting the results.
        <br />
        Please try again later.
      </p>
    </section>
  );
};

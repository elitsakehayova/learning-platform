import React from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'emotion-theming';
import moment from 'moment';
import { getNestedProperty } from '../utils';
import { tableRow, tableCell } from './styles';

const TableBody = props => {
  const { data, columns, testId = 'tableRow', theme } = props;

  const displayValue = (column, row) => {
    let value = getNestedProperty(row, column.value.split('.'));
    value =
      value && column.mapValue ? column.mapValue[value.toLowerCase()] : value;
    value =
      value && column.dateTimeFormat
        ? moment(value).format(column.dateTimeFormat)
        : value;

    return value || 'N/A';
  };

  return data.map(row => (
    <div
      key={row.id || row.code}
      className={tableRow(theme)}
      data-table-row-id={(row[testId] || testId).toLowerCase()}>
      {columns.map(column => (
        <div
          key={`${column.name}${row.id || row.code}`}
          className={tableCell(column.width, !!column.render)}
          data-table-column-id={column.name.toLowerCase()}>
          {column.render ? column.render(row) : displayValue(column, row)}
        </div>
      ))}
    </div>
  ));
};

TableBody.propTypes = {
  data: PropTypes.array,
  columns: PropTypes.array,
  theme: PropTypes.object,
  testId: PropTypes.string,
};

export default withTheme(TableBody);

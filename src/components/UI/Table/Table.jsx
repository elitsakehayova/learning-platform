import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { cx } from 'emotion';
import { isUndefined } from 'lodash';
import TableHead from './TableHead';
import TableBody from './TableBody';
import { Skeleton } from '../Loader';
import { NoResults, Error } from './NoResults';
import { tableContainer } from './styles';

const Table = props => {
  const {
    getDataMethod,
    columns,
    pageSize = 20,
    renderToolbar,
    hasPagination,
    className,
    testId,
  } = props;
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const tableOptions = useRef({
    pageSize,
    page: 1,
    totalPages: 1,
    skip: 0,
    take: pageSize * 5 + 1,
    sort: null,
    results: {},
  });

  useEffect(() => {
    !renderToolbar && getData();
  }, []);

  const getData = async (options = {}) => {
    setIsLoading(true);
    tableOptions.current = {
      ...tableOptions.current,
      ...options,
      ...(isUndefined(options.skip) && { skip: 0, results: [], totalPages: 0 }),
    };
    const { skip, take } = tableOptions.current;
    const [res, err] = await getDataMethod(tableOptions.current);

    if (hasPagination && res?.length) {
      tableOptions.current.results[options.skip || 0] = res.slice(0, take - 1);
      tableOptions.current.totalPages = Math.max(
        Math.ceil((res.length + skip) / pageSize) || 1,
        tableOptions.current.totalPages
      );
    }

    err && setError(err);
    res && setData(hasPagination ? res.slice(0, pageSize) : res);
    setIsLoading(false);
    return [res, err];
  };

  const handlePageChange = newPageIndex => {
    const { results } = tableOptions.current;
    const newSkip = Math.floor((newPageIndex - 1) / 5) * pageSize * 5;
    tableOptions.current.page = newPageIndex;
    tableOptions.current.skip = newSkip;

    const sliceStart = ((newPageIndex % 5 || 5) - 1) * pageSize;
    const sliceEnd = (newPageIndex % 5 || 5) * pageSize;

    !results[newSkip]
      ? getData({ skip: newSkip })
      : setData(results[newSkip].slice(sliceStart, sliceEnd));
  };

  const setTableOptions = newValue => {
    tableOptions.current = { ...tableOptions.current, ...newValue };
  };

  const handleSortChange = newSort => {
    tableOptions.current.sort = newSort;
    getData();
  };

  return (
    <div className={cx(tableContainer, className)} data-in-progress={isLoading}>
      {renderToolbar &&
        renderToolbar({
          setTableOptions,
          tableOptions,
          getData,
          handlePageChange,
          handleSortChange,
        })}
      <TableHead columns={columns} onSortRequest={handleSortChange} />
      {error ? (
        <Error />
      ) : !data ? (
        <Skeleton count={pageSize} height={32} marginBottom={12} />
      ) : !data.length ? (
        <NoResults />
      ) : (
        <TableBody data={data} columns={columns} testId={testId} />
      )}
    </div>
  );
};

Table.propTypes = {
  getDataMethod: PropTypes.func,
  columns: PropTypes.array,
  pageSize: PropTypes.number,
  renderToolbar: PropTypes.func,
  className: PropTypes.string,
  hasPagination: PropTypes.bool,
  testId: PropTypes.string,
};

export default Table;

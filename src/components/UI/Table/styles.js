import { css } from 'emotion';

export const tableContainer = css({
  display: 'flex',
  flexDirection: 'column',
});

export const tableHead = theme =>
  css({
    display: 'flex',
    flexFlow: 'row nowrap',
    cursor: 'pointer',
    borderTop: `1px solid ${theme.border}`,
    borderBottom: `1px solid ${theme.border}`,
  });

export const tableRow = theme =>
  css({
    display: 'flex',
    alignItems: 'stretch',

    '&:hover': {
      backgroundColor: theme.highlights,

      '& i': {
        color: theme.textLightPrimary,
      },
    },

    '& i': {
      color: theme.textLightDisabled,
    },
  });

export const tableCell = (width, hasRender) =>
  css({
    fontSize: '16px',
    lineHeight: '16px',
    flex: width ? `0 1 ${width}px` : '1 0 0%',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: hasRender ? 0 : '16px 6px',
    outline: 'none',
    cursor: 'default',
    wordBreak: 'break-word',
  });

export const sortableTableHeadCell = width =>
  css(
    {
      cursor: 'default',
      '& b': {
        cursor: 'pointer',
        '&:hover + i': {
          opacity: 1,
        },
      },
    },
    tableCell(width)
  );

export const tableHeadContent = css({
  fontSize: '14px',
});

export const tableHeadIcon = (isAsc, isDesc) =>
  css({
    fontSize: 18,
    opacity: isAsc || isDesc ? 1 : 0,
    transform: `rotate(${isDesc ? 0 : 180}deg)`,
    transition: 'transform 300ms',
  });

export const tableCellContent = css({
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',

  '& span': {
    display: 'inline',
  },
});

export const paginationContainer = css({
  display: 'flex',
  alignItems: 'center',
  flexWrap: 'nowrap',
  '& div': {
    borderRadius: '50%',
  },
});

export const paginationIcon = css({
  width: 40,
  height: 40,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  cursor: 'pointer',
  fontSize: 28,
});

export const disabledIcon = theme =>
  css(paginationIcon, {
    cursor: 'not-allowed',
    color: theme.textLightDisabled,
  });

export const shownResultsIndicator = theme =>
  css({
    color: theme.textLightSecondary,
    width: 80,
    textAlign: 'center',
    fontSize: 12,
  });

export const noResultsContainer = css({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  padding: '80px 20px',
  textAlign: 'center',
  flexDirection: 'column',
});

export const noResultsIcon = theme =>
  css({
    width: 120,
    height: 120,
    backgroundColor: theme.primaryVeryLight,
    marginBottom: 16,
    borderRadius: '50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  });

export const noResultsTitle = css({
  fontWeight: 500,
  marginBottom: 24,
});

export const noResultsSubtitle = theme =>
  css({
    color: theme.textLightSecondary,
  });

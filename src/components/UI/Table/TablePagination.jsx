import React, { useState, useImperativeHandle, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from 'emotion-theming';
import Icon from '../Icon';
import Ripple from '../Ripple';
import {
  paginationContainer,
  paginationIcon,
  disabledIcon,
  shownResultsIndicator,
} from './styles';

const Pagination = forwardRef(({ tableOptions, onPageChange }, ref) => {
  const theme = useTheme();
  const [page, setPage] = useState(1);
  const { skip, results, totalPages, pageSize } = tableOptions.current;
  const resultsKeys = Object.keys(results);
  const noResults = resultsKeys.indexOf(skip.toString()) === -1;

  useImperativeHandle(ref, () => ({ page, setPage }));

  const handlePageChange = newPage => {
    onPageChange(newPage);
    setPage(newPage);
  };

  const baseIcon = (disabled, type) => (
    <Icon
      iconName={type === 'left' ? 'chevron_left' : 'chevron_right'}
      className={disabled ? disabledIcon(theme) : paginationIcon}
      {...(!disabled && {
        onClick: () => handlePageChange(page - (type === 'left' ? 1 : -1)),
      })}
      data-id={type === 'left' ? 'tablepreviouspage' : 'tablenextpage'}
    />
  );

  const withRipple = type => (
    <Ripple flat backColor="grayLight">
      {baseIcon(false, type)}
    </Ripple>
  );

  const iconComponent = (noRipple, type) =>
    noRipple ? baseIcon(true, type) : withRipple(type);

  return (
    <div className={paginationContainer}>
      <div className={shownResultsIndicator(theme)} data-id="tabletotalpages">
        {`${page * pageSize - (pageSize - 1)} - ${page * pageSize}`}
      </div>
      {iconComponent(page === 1 || noResults, 'left')}
      {iconComponent(page === totalPages || noResults, 'right')}
    </div>
  );
});

Pagination.propTypes = {
  tableOptions: PropTypes.object,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;

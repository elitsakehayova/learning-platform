import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'emotion-theming';
import Icon from '../Icon';
import {
  tableHead,
  tableCell,
  sortableTableHeadCell,
  tableHeadContent,
  tableHeadIcon,
} from './styles';

const TableHead = props => {
  const { columns, onSortRequest, theme } = props;
  const [sort, setSort] = useState(null);

  const changeSort = column => () => {
    const { value } = column;
    const newSort =
      sort === value
        ? `${value}Descending`
        : sort === `${value}Descending`
        ? null
        : value;
    setSort(newSort);
    onSortRequest(newSort);
  };

  return (
    <div className={tableHead(theme)}>
      {columns.map(column => {
        const { sortable, name, value, width } = column;
        return sortable ? (
          <div
            key={name}
            data-table-head-id={name.toLowerCase()}
            className={sortableTableHeadCell(width)}
            role="button"
            tabIndex="-1"
            onClick={changeSort(column)}>
            <b className={tableHeadContent}>{name}</b>
            <Icon
              iconName="arrow_downward"
              className={tableHeadIcon(
                sort === value,
                sort === `${value}Descending`
              )}
            />
          </div>
        ) : (
          <div
            key={name}
            data-table-head-id={name.toLowerCase()}
            className={tableCell(width)}>
            <b className={tableHeadContent}>{name}</b>
          </div>
        );
      })}
    </div>
  );
};

TableHead.propTypes = {
  columns: PropTypes.array,
  onSortRequest: PropTypes.func,
  theme: PropTypes.object,
};

export default withTheme(TableHead);

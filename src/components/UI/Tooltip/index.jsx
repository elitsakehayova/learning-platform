import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from 'emotion-theming';
import { isFunction } from 'lodash';
import { tooltipContainer, popupTooltip } from './styles';

const Tooltip = props => {
  const { children, content, onClick, open, style } = props;
  const theme = useTheme();

  const [isOpen, setIsOpen] = useState(open);

  const handleClick = () => onClick && setIsOpen(prev => !prev);

  const handleBlur = () => onClick && setIsOpen(false);

  return (
    <div
      role="presentation"
      onClick={handleClick}
      onBlur={handleBlur}
      className={tooltipContainer(props, isOpen)}
      style={style}>
      <div className={popupTooltip(props, theme)}>
        {isFunction(content) ? content() : content}
      </div>
      {children}
    </div>
  );
};

Tooltip.propTypes = {
  children: PropTypes.any,
  content: PropTypes.any,
  open: PropTypes.bool,
  onClick: PropTypes.bool,
  style: PropTypes.object,
};

export default Tooltip;

import { css } from 'emotion';

export const header = css({
  cursor: 'pointer',
});

export const content = (isOpen, height) =>
  css({
    height: isOpen ? height : 0,
    overflow: 'hidden',
    transition: 'height 0.4s ease-in-out',
  });

export const container = css({
  display: 'flex',
  flexDirection: 'column',
});

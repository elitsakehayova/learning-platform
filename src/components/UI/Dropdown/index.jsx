import React, { useEffect, forwardRef, useImperativeHandle } from 'react';
import PropTypes from 'prop-types';
import { isEmpty, isArray } from 'lodash';
import { cx } from 'emotion';
import { useTheme } from 'emotion-theming';
import hash from 'object-hash';
import OptionsList from './OptionsList';
import Icon from '../Icon';
import { useDropDown } from './useDropDown';
import { removeNodes, prepareOptions } from './utils';
import { noValue } from '../utils';
import {
  dropDownContainer,
  dropDownSelect,
  dropDownOptionsContainer,
  dropDownRightIcon,
  dropDownText,
  dropDownDeleteIcon,
  rightIconsContainer,
} from './styles';

const Dropdown = forwardRef((props, ref) => {
  const {
    leftIcon,
    autoWidth,
    multiSelect,
    onTop,
    onLeft,
    options: initOptions = [],
    maxOptions = 7,
    disabled: initDisabled,
    className,
    withSearch,
    displayKey = 'name',
    uniqueKey = 'code',
    noPrefilled,
    noClear,
    testId = 'dropdown',
  } = props;

  const theme = useTheme();
  const options = prepareOptions(initOptions, displayKey);
  const disabled = !options || !options.length || initDisabled;

  const {
    openClose,
    handleBlur,
    handleSelect,
    handleClear,
    isOpen,
    selected,
    displayValue,
    selectRef,
    width,
    calculateDisplayValue,
  } = useDropDown({ ...props, options });

  // If there is only one element preselect it
  useEffect(() => {
    if (!noValue(selected) || noPrefilled) return;
    const optionsList =
      isArray(initOptions) && initOptions.filter(el => !el.groupLabel);
    optionsList.length === 1 && handleSelect(optionsList[0]);
  }, [hash({ options: removeNodes(options) })]);

  useImperativeHandle(ref, () => ({
    changeValue: handleSelect,
    value: selected,
    displayValue,
    clear: handleClear,
  }));

  const numberOfOptions = options.length;

  const isSelected = multiSelect ? selected.length : !isEmpty(selected);

  return (
    <div
      role="menuitem"
      tabIndex={0}
      onBlur={handleBlur}
      className={dropDownContainer(autoWidth, disabled)}
      onClick={openClose}
      data-id={testId.toLowerCase()}>
      <div
        className={cx(
          dropDownSelect(isOpen, isSelected, autoWidth, width, theme),
          className
        )}>
        {leftIcon && <Icon {...leftIcon} />}
        <span
          className={dropDownText(isSelected)}
          title={displayValue || undefined}>
          {calculateDisplayValue()}
        </span>
        <div className={rightIconsContainer}>
          {!!isSelected && !noClear && !disabled && (
            <Icon
              iconName="close"
              className={dropDownDeleteIcon(theme)}
              onClick={handleClear}
            />
          )}
          <Icon
            iconName="keyboard_arrow_down"
            color="textLightSecondary"
            className={dropDownRightIcon(isOpen)}
          />
        </div>
      </div>
      <OptionsList
        isOpen={isOpen}
        displayKey={displayKey}
        uniqueKey={uniqueKey}
        withSearch={withSearch}
        multiSelect={multiSelect}
        options={options}
        selected={selected}
        onSelect={handleSelect}
        className={dropDownOptionsContainer(
          isOpen,
          autoWidth,
          numberOfOptions,
          maxOptions,
          onTop,
          onLeft,
          theme
        )}
        containerRef={selectRef}
        theme={theme}
        testId={testId}
      />
    </div>
  );
});

Dropdown.propTypes = {
  leftIcon: PropTypes.object,
  autoWidth: PropTypes.bool,
  multiSelect: PropTypes.bool,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  mapValue: PropTypes.func,
  value: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.string,
    PropTypes.array,
  ]),
  options: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        values: PropTypes.array,
      })
    ),
  ]),
  onTop: PropTypes.bool,
  onLeft: PropTypes.bool,
  maxOptions: PropTypes.number,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  withSearch: PropTypes.bool,
  noSimplify: PropTypes.bool,
  displayKey: PropTypes.string,
  uniqueKey: PropTypes.string,
  noPrefilled: PropTypes.bool,
  noClear: PropTypes.bool,
  testId: PropTypes.string,
};

export default Dropdown;

import { useState, useRef, useEffect, useLayoutEffect } from 'react';
import { isEqual, isEmpty, isNil } from 'lodash';
import hash from 'object-hash';
import { outputValue, removeNodes, prepareValue } from './utils';

export const useDropDown = ({
  value: initValue,
  options,
  onChange,
  onBlur,
  open,
  alwaysFocused,
  multiSelect,
  autoWidth,
  disabled,
  mapValue,
  displayKey = 'name',
  uniqueKey = 'code',
  placeholder = '',
  noSimplify,
}) => {
  const allKeys = [uniqueKey, displayKey];

  const selectedValue = prepareValue(
    initValue,
    options,
    multiSelect,
    displayKey,
    uniqueKey
  );
  const [selected, setSelected] = useState(selectedValue);
  const [isOpen, setIsOpen] = useState(false);
  const selectRef = useRef({
    parentElement: null,
    offsetWidth: 0,
  });
  const [selectWidth, setSelectWidth] = useState(null);
  const isDisabled = !options?.length || disabled;

  useEffect(() => {
    open && setIsOpen(true);
  }, []);

  useEffect(() => {
    setTimeout(() => {
      alwaysFocused && isOpen && selectRef.current?.parentElement?.focus();
    });
  });

  useEffect(() => {
    setSelected(selectedValue);
  }, [hash({ selectedValue: removeNodes(selectedValue) })]);

  useLayoutEffect(() => {
    calculateWidth();
  }, [selectRef.current.offsetWidth]);

  const calculateWidth = () => {
    // Make the width of the dropdown dynamic
    // Calculate its width base on the width of the container with all options
    const select = selectRef.current;
    if (!autoWidth) setSelectWidth(select.offsetWidth);
  };

  const openClose = () => !isDisabled && setIsOpen(prev => !prev);

  const areEquals = (source, value) => {
    // Check if the values are defined because isEqual with 2 undefined returns true
    const areDef = key => !isNil(source[key]) && !isNil(value[key]);
    const areSame = key => isEqual(source[key], value[key]);
    const compare = key => areDef(key) && areSame(key);
    return allKeys.some(compare);
  };

  const handleBlur = ({ currentTarget }) =>
    setTimeout(() => {
      if (!currentTarget.contains(document.activeElement) && isOpen) {
        setIsOpen(false);
        onBlur && onBlur();
      }
    });

  const handleSelect = newValue => {
    if (multiSelect) {
      if (isNil(newValue)) return changeValue([]);
      const selectedUnique = selected.filter(el => !isNil(el));
      const valueIndex = selectedUnique.findIndex(e => areEquals(e, newValue));

      if (valueIndex === -1) return changeValue([...selectedUnique, newValue]);

      selectedUnique.splice(valueIndex, 1);
      return changeValue(selectedUnique);
    }
    setIsOpen(false);
    return changeValue(newValue);
  };

  const handleClear = e => {
    e.stopPropagation();
    handleSelect(null);
  };

  const changeValue = newValue => {
    if (isEqual(newValue, selected)) return;
    setSelected(newValue);
    setTimeout(() => onChange(outputValue(newValue, mapValue, !noSimplify)));

    // Focus the dropdown so it can trigger onBlur function which will check for error
    // This is for the case where the user open the dropdown and do not select nothing (if the field is required is should show error)
    isOpen && selectRef.current?.parentElement?.focus();
  };

  const getOptionValue = val => val && val[displayKey];

  const hasSelection = multiSelect ? selected.length : !isEmpty(selected);

  const displayValue = multiSelect
    ? selected.map(getOptionValue).join(', ')
    : getOptionValue(selected);

  const calculateDisplayValue = () =>
    hasSelection ? displayValue : placeholder;

  return {
    openClose,
    handleBlur,
    handleSelect,
    handleClear,
    isOpen,
    selected,
    displayValue,
    width: selectWidth,
    selectRef,
    calculateDisplayValue,
  };
};

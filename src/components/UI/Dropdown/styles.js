import { css } from 'emotion';

const itemHeight = 51;

export const dropDownContainer = (autoWidth, disabled) =>
  css({
    position: 'relative',
    cursor: disabled ? 'not-allowed' : 'pointer',
    opacity: disabled ? 0.3 : 1,
    outline: 'none',
    fontSize: '16px',
    display: 'flex',
    flexDirection: 'column',
    width: autoWidth ? '100%' : 'auto',
  });

export const dropDownSelect = (
  isOpened,
  isSelected,
  autoWidth,
  width,
  theme
) => {
  const borderColor = isSelected ? theme.primary : theme.border;
  return css({
    border: `1px solid ${isOpened ? 'transparent' : borderColor}`,
    borderRadius: isOpened ? '4px 4px 0 0' : '4px',
    boxShadow: isOpened ? '0 2px 4px rgba(0,0,0,0.3)' : 'none',
    minWidth: 150,
    width: (autoWidth && '100%') || width,
    maxWidth: autoWidth ? 'none' : 250,
    padding: '10px 8px 10px 12px',
    color: theme[isSelected ? 'textLightPrimary' : 'textLightDisabled'],
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  });
};

export const dropDownText = isSelected =>
  css({
    userSelect: isSelected ? 'text' : 'none',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  });

export const dropDownOptionsContainer = (
  isOpened,
  autoWidth,
  numberOfItems,
  maxNumber,
  onTop,
  onLeft,
  theme
) =>
  css(
    {
      position: 'absolute',
      zIndex: 150,
      width: autoWidth ? '100%' : 'auto',
      color: theme.textLightSecondary,
      height: 'auto',
      maxHeight:
        numberOfItems > maxNumber
          ? maxNumber * itemHeight - itemHeight / 2
          : 'none',
      overflowY: numberOfItems > maxNumber ? 'auto' : 'hidden',
      backgroundColor: 'white',
      transform: `scaleY(${isOpened ? 1 : 0})`,
      transformOrigin: onTop ? 'bottom' : 'top',
      transition: 'transform 400ms ease-in-out',
      cursor: 'default',
    },
    onTop
      ? {
          bottom: '100%',
          boxShadow: '0 -2px 4px rgba(0,0,0,0.3)',
          borderBottom: isOpened ? '1px solid #DDDDDD' : 'none',
          borderRadius: '4px 4px 0 0',
        }
      : {
          top: '100%',
          boxShadow: '0 4px 4px rgba(0,0,0,0.3)',
          borderTop: isOpened ? '1px solid #DDDDDD' : 'none',
          borderRadius: '0 0 4px 4px',
        },
    onLeft && { right: 0 }
  );

export const dropDownOptionItem = (isSelected, theme) =>
  css({
    padding: '16px 32px 16px 16px',
    color: theme.textLightPrimary,
    backgroundColor: isSelected ? theme.secondaryHover : 'white',
    display: 'inline-flex',
    alignItems: 'center',
    width: '100%',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: theme.secondaryHover,
    },
    '&:focus': {
      outline: 'none',
    },
    '& i': {
      marginRight: 8,
    },
  });

export const dropDownGroupLabel = theme =>
  css({
    margin: 0,
    padding: 12,
    cursor: 'default',
    color: theme.textLightSecondary,
    fontSize: 14,
    fontWeight: 400,
  });

export const rightIconsContainer = css({
  display: 'flex',
});

export const dropDownRightIcon = isOpened =>
  css({
    fontSize: '19px',
    transform: `rotate(${isOpened ? -180 : 0}deg)`,
    transition: 'transform 0.35s ease-out',
    cursor: 'pointer',
    display: 'inline-flex',
  });

export const dropDownDeleteIcon = theme =>
  css({
    display: 'inline-flex',
    fontSize: '19px',
    cursor: 'pointer',
    color: theme.textLightSecondary,

    '&:hover': {
      color: theme.textLightPrimary,
    },
  });

export const dropdownSearch = theme =>
  css({
    position: 'sticky',
    top: 0,
    backgroundColor: theme.white,
    borderBottom: `1px solid ${theme.grayLight}`,
  });

export const noResultsContainer = css({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '32px 8px',
  '& h4': {
    margin: '8px 0 12px 0',
  },
});

export const noResultsIcon = theme =>
  css({
    backgroundColor: theme.primaryVeryLight,
    borderRadius: '50%',
    padding: 8,
  });

export const infiniteScroll = css({
  padding: 16,
  display: 'flex',
  justifyContent: 'center',
});

import { css } from 'emotion';

export const buttonContainer = css({
  outline: 'none',
  display: 'inline-flex',
});

export const baseButton = ({ theme, disabled, color, backColor }) =>
  css({
    borderRadius: 22,
    cursor: disabled ? 'not-allowed' : 'pointer',
    padding: '11px 35px',
    color: theme[color] || theme.secondary,
    textTransform: 'uppercase',
    fontSize: 14,
    fontWeight: 500,
    letterSpacing: 0.5,
    lineHeight: 1,
    textAlign: 'center',
    opacity: disabled ? 0.4 : 1,
    backgroundColor: theme[backColor] || theme.primary,
    userSelect: 'none',
    outline: 'none',
    display: 'flex',
    alignItems: 'center',
    whiteSpace: 'nowrap',

    '&:hover': {
      backgroundColor: theme[disabled ? 'primary' : 'primaryHover'],
    },

    '& > i': {
      marginRight: 6,
    },
  });

export const secondaryButton = ({ theme, disabled }) =>
  css({
    color: theme.primary,
    border: `1px solid ${theme.primary}`,
    backgroundColor: theme.secondary,
    '&:hover': {
      backgroundColor: theme[disabled ? 'secondary' : 'secondaryHover'],
    },
  });

export const clearButton = css({
  backgroundColor: 'transparent',
  borderColor: 'transparent',
  textAlign: 'left',
});

export const smallButton = css({
  fontSize: 12,
  padding: '8px 16px',
});

export const largeButton = css({
  fontSize: 16,
  padding: '15px 55px',
});

export const checkButtonContainer = css({
  display: 'inline-flex',
  alignItems: 'center',
});

export const checkBoxContainer = theme =>
  css({
    width: '32px',
    height: '32px',
    borderRadius: '50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    userSelect: 'none',
    '&:hover': {
      backgroundColor: theme.secondaryHover,
    },
  });

export const buttonDropdownContainer = css({
  position: 'relative',
  outline: 'none',
  fontSize: '16px',
});

const itemHeight = 51;

export const buttonDropdownOptionsContainer = (
  isOpened,
  maxNumber,
  numberOfItems,
  optionListClassName,
  theme
) =>
  css(
    {
      position: 'absolute',
      top: '100%',
      right: 0,
      zIndex: 150,
      width: 'auto',
      color: theme.textLightSecondary,
      height: 'auto',
      maxHeight: maxNumber * itemHeight - itemHeight / 2,
      overflowY: numberOfItems > maxNumber ? 'auto' : 'hidden',
      backgroundColor: 'white',
      transform: `scaleY(${isOpened ? 1 : 0})`,
      transformOrigin: 'top',
      transition: 'transform 400ms ease-in-out',
      boxShadow: '0 4px 4px rgba(0,0,0,0.3)',
      borderTop: isOpened ? '1px solid #DDDDDD' : 'none',
      borderRadius: '0 0 4px 4px',
    },
    optionListClassName
  );

export const buttonDropdownOptionItem = theme =>
  css({
    padding: '16px 32px 16px 16px',
    color: theme.textLightPrimary,
    backgroundColor: 'transparent',
    display: 'inline-flex',
    alignItems: 'center',
    whiteSpace: 'nowrap',
    width: '100%',
    '&:focus': {
      outline: 'none',
    },
    '& i, & img': {
      marginRight: 10,
    },
  });

export const switchButtonContainer = width =>
  css({
    position: 'relative',
    width: width || 70,
    userSelect: 'none',
  });

export const switchButtonInput = css({
  display: 'none',
});

export const switchButtonLabel = css({
  display: 'block',
  overflow: 'hidden',
  cursor: 'pointer',
  width: '100%',
  borderRadius: 20,
});

export const switchButtonInner = (
  isChecked,
  activeText,
  inactiveText,
  {
    backgroundColorBefore,
    activeTextColor,
    backgroundColorAfter,
    inactiveTextColor,
    fontSizeLabels,
    textSpacingLabels,
    sizingHeight,
  },
  theme
) =>
  css({
    display: 'block',
    width: '200%',
    marginLeft: isChecked ? 0 : '-100%',
    transition: 'margin 0.3s ease-in 0s',

    '&:before, &:after': {
      display: 'inline-block',
      width: '50%',
      height: sizingHeight || 30,
      padding: 0,
      lineHeight: `${sizingHeight || 30}px`,
      fontSize: fontSizeLabels || 14,
      color: 'white',
      fontWeight: 'bold',
    },

    '&:before': {
      content: activeText ? `'${activeText}'` : "'ON'",
      paddingLeft: textSpacingLabels || 10,
      backgroundColor: backgroundColorBefore || theme.primary,
      color: activeTextColor || theme.white,
    },

    '&:after': {
      content: inactiveText ? `'${inactiveText}'` : "'OFF'",
      paddingRight: textSpacingLabels || 10,
      backgroundColor: backgroundColorAfter || theme.grayLight,
      color: inactiveTextColor || theme.gray,
      textAlign: 'right',
    },
  });

export const switchButtonCircle = (
  isChecked,
  width,
  { switchSize, switchBackgroundColor, switchBorderColor },
  theme
) =>
  css({
    display: 'block',
    width: switchSize || 20,
    height: switchSize || 20,
    background: switchBackgroundColor || theme.white,
    position: 'absolute',
    bottom: 5,
    right: isChecked ? 4 : (width || 70) - (switchSize || 24),
    border: `2px solid ${switchBorderColor || theme.grey} `,
    borderRadius: 20,
    transition: 'all 0.3s ease-in 0s',
  });

export const checkGroup = css({
  '& > *': {
    marginRight: 8,
  },
});

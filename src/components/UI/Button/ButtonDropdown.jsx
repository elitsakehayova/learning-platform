import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'emotion-theming';
import Button from './Button';
import Ripple from '../Ripple';
import {
  buttonDropdownContainer,
  buttonDropdownOptionsContainer,
  buttonDropdownOptionItem,
} from './styles';

const ButtonDropdown = ({
  children,
  options = [],
  maxOptions = 7,
  disabled = !options.length,
  theme,
  optionListClassName,
  ...buttonProps
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleBlur = ({ currentTarget }) =>
    setTimeout(() => {
      if (!currentTarget.contains(document.activeElement) && isOpen)
        setIsOpen(false);
    });

  const renderBaseOptionElement = (option, i) => {
    const optionRender = option.render ? option.render : option.label;
    return (
      <div
        key={`buttonDropdownItem${i * Math.random()}`}
        role="presentation"
        onClick={option.onClick}>
        <Ripple
          backColor="secondaryPressed"
          className={buttonDropdownOptionItem(theme)}>
          {optionRender}
        </Ripple>
      </div>
    );
  };

  return (
    <div className={buttonDropdownContainer} onBlur={handleBlur}>
      <Button
        {...buttonProps}
        disabled={disabled}
        onClick={() => setIsOpen(!isOpen)}>
        {children}
      </Button>
      <div
        className={buttonDropdownOptionsContainer(
          isOpen,
          maxOptions,
          options.length,
          optionListClassName,
          theme
        )}>
        {options.map(renderBaseOptionElement)}
      </div>
    </div>
  );
};

ButtonDropdown.propTypes = {
  options: PropTypes.array,
  children: PropTypes.any,
  theme: PropTypes.object,
  maxOptions: PropTypes.number,
  disabled: PropTypes.bool,
  optionListClassName: PropTypes.string,
};

export default withTheme(ButtonDropdown);

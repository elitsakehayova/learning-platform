import React from 'react';
import PropTypes from 'prop-types';
import { cx } from 'emotion';
import { withTheme } from 'emotion-theming';
import Icon from '../Icon';
import Ripple from '../Ripple';
import {
  baseButton,
  secondaryButton,
  buttonContainer,
  clearButton,
  smallButton,
  largeButton,
} from './styles';

const Button = props => {
  const {
    children,
    onClick,
    disabled,
    leftIcon,
    secondary,
    clear,
    small,
    large,
    className,
    noPreventDefault,
  } = props;

  const classes = cx(
    baseButton(props),
    (secondary || clear) && secondaryButton(props),
    clear && clearButton,
    small && smallButton,
    large && largeButton,
    className
  );

  const handleClick = e => {
    !noPreventDefault && e.preventDefault();
    if (disabled) return;
    onClick && onClick();
  };

  return (
    <div
      tabIndex="0"
      role="button"
      onClick={handleClick}
      className={buttonContainer}>
      {disabled ? (
        <div className={classes}>{children}</div>
      ) : (
        <Ripple
          backColor={secondary || clear ? 'secondaryPressed' : 'primaryPressed'}
          focusColor={secondary || clear ? 'secondaryFocus' : 'primaryFocus'}
          className={classes}>
          {leftIcon && <Icon {...leftIcon} />}
          {children}
        </Ripple>
      )}
    </div>
  );
};

Button.propTypes = {
  children: PropTypes.any,
  disabled: PropTypes.bool,
  secondary: PropTypes.bool,
  onClick: PropTypes.func,
  noPreventDefault: PropTypes.bool,
  color: PropTypes.string,
  backColor: PropTypes.string,
  clear: PropTypes.bool,
  small: PropTypes.bool,
  large: PropTypes.bool,
  className: PropTypes.string,
  leftIcon: PropTypes.object,
  theme: PropTypes.object,
};

export default withTheme(Button);

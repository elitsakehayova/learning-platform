import React, { useState, useEffect } from 'react';
import { useTheme } from 'emotion-theming';
import PropTypes from 'prop-types';
import {
  switchButtonContainer,
  switchButtonInput,
  switchButtonLabel,
  switchButtonInner,
  switchButtonCircle,
} from './styles';

const SwitchButton = props => {
  const {
    id,
    style = {},
    value,
    onChange = () => {},
    width,
    activeText,
    inactiveText,
  } = props;
  const theme = useTheme();
  const [isChecked, setIsChecked] = useState(!!value);
  const inputId = id || Math.random();

  useEffect(() => {
    setIsChecked(value);
  }, [value]);

  const handleChange = () =>
    setIsChecked(prev => {
      onChange(!prev);
      return !prev;
    });

  return (
    <div className={switchButtonContainer(width)}>
      <input
        className={switchButtonInput}
        type="checkbox"
        name="onOffSwitch"
        onChange={handleChange}
        id={inputId}
        checked={isChecked}
      />
      <label className={switchButtonLabel} htmlFor={inputId}>
        <span
          className={switchButtonInner(
            isChecked,
            activeText,
            inactiveText,
            style,
            theme
          )}
        />
        <span className={switchButtonCircle(isChecked, width, style, theme)} />
      </label>
    </div>
  );
};
SwitchButton.propTypes = {
  id: PropTypes.string,
  style: PropTypes.object,
  value: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  width: PropTypes.number,
  activeText: PropTypes.string,
  inactiveText: PropTypes.string,
};
export default SwitchButton;

import React from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'emotion-theming';
import Icon from '../Icon';
import Ripple from '../Ripple';
import { checkButtonContainer, checkBoxContainer } from './styles';

const Checkbox = ({ checked, onChange, radio, theme, children }) => {
  const iconName = radio
    ? checked
      ? 'radio_button_checked'
      : 'radio_button_unchecked'
    : checked
    ? 'check_box'
    : 'check_box_outline_blank';

  const handleChange = () => onChange(!checked);

  return (
    <div
      role="button"
      tabIndex={0}
      className={checkButtonContainer}
      onClick={handleChange}>
      <Ripple
        flat
        backColor="secondaryPressed"
        focusColor="secondaryFocus"
        className={checkBoxContainer(theme)}>
        <Icon iconName={iconName} color="primary" />
      </Ripple>
      {children}
    </div>
  );
};

Checkbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  radio: PropTypes.bool,
  theme: PropTypes.object,
  children: PropTypes.any,
};

export default withTheme(Checkbox);

/* eslint-disable no-bitwise */
export default () => {
  const pattern = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx';
  return pattern.replace(/x/g, () => ((Math.random() * 16) | 0).toString(16));
};

import {
  isNil,
  isString,
  isNumber,
  isArray,
  isPlainObject,
  isEmpty,
} from 'lodash';

export const noValue = val =>
  isNil(val) ||
  ((isString(val) || isArray(val) || isPlainObject(val)) && isEmpty(val));

export const isStrNum = val => isString(val) || isNumber(val);

export const generateName = (employee = {}) => {
  if (!employee) return null;
  const { firstName, middleName, lastName } = employee;
  return (
    (firstName || '')
      .concat(middleName ? ` ${middleName}` : '')
      .concat(lastName ? ` ${lastName}` : '') || 'N/A'
  );
};

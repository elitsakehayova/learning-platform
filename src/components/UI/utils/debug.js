import { isFunction } from 'lodash';

export const isDev = env === 'development';
export const isQA = env === 'qa';
export const isProd = !isDev && !isQA;

export const ifDev = callback => isDev && isFunction(callback) && callback();
export const ifQAorDev = callback =>
  (isDev || isQA) && isFunction(callback) && callback();
export const ifQAOnly = callback => isQA && isFunction(callback) && callback();
export const ifProd = callback => isProd && isFunction(callback) && callback();

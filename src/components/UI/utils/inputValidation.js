/* eslint-disable no-restricted-syntax */
/* eslint-disable max-len */
import { flattenDeep, isNil, isEmpty, isFunction, isObject } from 'lodash';

const validators = {
  required: {
    regExp: /\S/,
    msg: 'This field is required',
  },
  email: {
    regExp: /^$|^(([^<>()\\.,;:\s@"]+(\.[^<>()\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    msg: 'Invalid Email',
  },
  username: {
    regExp: /^$|^[A-Za-z0-9_+\-.!#$'^`~@]{1,25}$/,
    msg: 'Invalid username',
  },
  password: {
    regExp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%^&*]).{8,}$/,
    msg: 'Invalid password',
  },
  phone: {
    regExp: /^$|^[6-9][0-9]{9}$/,
    msg: 'Invalid phone number',
  },
  postCode: {
    regExp: /^$|^[0-9]{6}$/,
    msg: 'Invalid post code',
  },
};

export default (value, ...inputValidators) => {
  let error = null;
  const setOfValidators = flattenDeep(inputValidators);

  for (const validator of setOfValidators) {
    const validatorName = validator?.name ?? validator;
    const render = validator?.render;
    const { regExp, msg } = validators[validatorName];

    error =
      // Bypass null/undefined because the regExp api is stupid and if you pass it null/undefined it will convert them to string
      regExp.test(isNil(value) ? '' : value) ||
      // Check if the value is object or array and if it is check for emptiness
      (isObject(value) && !isEmpty(value))
        ? null
        : {
            msg,
            ...(isFunction(render) && { render }),
          };
    if (error) break;
  }
  return error;
};

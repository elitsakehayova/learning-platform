/* eslint-disable no-bitwise */
import { invert, isArray } from 'lodash';

export const extractBinary = (value, possibleValues) =>
  Object.values(possibleValues)
    .filter(el => (el & value) === Number(el))
    .map(el => invert(possibleValues)[el]);

export const accumulateBinary = (value, possibleValues) => {
  if (!isArray(value)) return possibleValues[value];
  return value.reduce((acc, val) => acc | possibleValues[val], 0);
};

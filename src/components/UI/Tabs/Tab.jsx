import React from 'react';
import PropTypes from 'prop-types';
import { cx } from 'emotion';
import { withTheme } from 'emotion-theming';
import Ripple from '../Ripple';
import { baseTab, activeTab, inactiveTab } from './styles';

const Tab = props => {
  const { label, active, onClick, theme } = props;
  const className = cx(inactiveTab(theme), active && activeTab(theme));

  const handleClick = () => onClick(label);

  return (
    <div role="presentation" className={className} onClick={handleClick}>
      <Ripple className={baseTab(theme)} backColor="secondaryPressed">
        {label}
      </Ripple>
    </div>
  );
};

Tab.propTypes = {
  label: PropTypes.string.isRequired,
  active: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  theme: PropTypes.object,
};

export default withTheme(Tab);

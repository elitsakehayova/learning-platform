import { css } from 'emotion';

export const tabs = css({
  width: '100%',
});

export const tabsListContainerWrapper = showArrows =>
  css(
    {
      position: 'relative',
      overflow: 'hidden',
      borderBottom: '1px solid #EDEFF1',
    },
    showArrows && { paddingRight: 90 }
  );

export const tabsListContainer = isDragging =>
  css(
    {
      overflowX: 'hidden',
      whiteSpace: 'nowrap',
      marginBottom: -50,
      paddingBottom: 50,
      display: 'flex',

      '& > *': {
        display: 'inline-block',
      },
    },
    isDragging && {
      '& *': {
        cursor: 'grabbing',

        '&:hover': {
          backgroundColor: 'transparent',
        },
      },
    }
  );

export const baseTab = theme =>
  css({
    cursor: 'pointer',
    padding: '10px 24px',
    textTransform: 'uppercase',
    fontSize: '14px',
    fontWeight: 500,
    transition: 'border-color 400ms',
    '&:hover': {
      backgroundColor: theme.secondaryHover,
    },
  });

export const inactiveTab = theme =>
  css({
    '& > *': {
      color: theme.textLightSecondary,
      backgroundColor: theme.secondary,
    },
  });

export const activeTab = theme =>
  css({
    '& > *': {
      color: theme.primary,
      borderBottom: `2px solid ${theme.primary}`,
    },
  });

const arrowStyle = (theme, isDisabled) => ({
  width: 36,
  height: 36,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  fontSize: 26,
  color: isDisabled ? theme.textLightDisabled : theme.textLightPrimary,
  borderRadius: '50%',
  cursor: isDisabled ? 'not-allowed' : 'pointer',
});

export const scrollLeftIcon = (theme, isDisabled) =>
  css({
    position: 'absolute',
    top: 0,
    right: 44,
    ...arrowStyle(theme, isDisabled),
  });

export const scrollRightIcon = (theme, isDisabled) =>
  css({
    position: 'absolute',
    top: 0,
    right: 0,
    ...arrowStyle(theme, isDisabled),
  });

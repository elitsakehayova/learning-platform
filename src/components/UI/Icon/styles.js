import { css } from 'emotion';

export default (color, hasOnClick, size) =>
  css(
    {
      fontFamily: "'Material Icons'",
      fontWeight: 'normal',
      fontStyle: 'normal',
      fontSize: size || 24,
      display: 'table',
      lineHeight: '1',
      textTransform: 'none',
      letterSpacing: 'normal',
      wordWrap: 'normal',
      whiteSpace: 'nowrap',
      direction: 'ltr',
      color,
      WebkitFontSmoothing: 'antialiased',
      textRendering: 'optimizeLegibility',
      MozOsxFontSmoothing: 'grayscale',
      fontFeatureSettings: "'liga'",
      userSelect: 'none',
    },
    hasOnClick && {
      cursor: 'pointer',
    }
  );

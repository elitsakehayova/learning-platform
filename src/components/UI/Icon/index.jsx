import React from 'react';
import PropTypes from 'prop-types';
import { cx } from 'emotion';
import { withTheme } from 'emotion-theming';
import { isUndefined } from 'lodash';
import iconStyle from './styles';

const Icon = props => {
  const {
    iconName,
    color,
    size,
    className,
    onClick,
    theme,
    ...restProps
  } = props;

  return (
    <i
      role="presentation"
      className={cx(
        iconStyle(theme[color] || color, !isUndefined(onClick), size),
        className
      )}
      onClick={onClick}
      {...restProps}>
      {iconName}
    </i>
  );
};

Icon.propTypes = {
  iconName: PropTypes.string.isRequired,
  color: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  size: PropTypes.number,
  theme: PropTypes.object,
};

export default withTheme(Icon);

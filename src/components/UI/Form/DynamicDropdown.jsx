import React, {
  useRef,
  useState,
  useEffect,
  useMemo,
  forwardRef,
  useImperativeHandle,
} from 'react';
import PropTypes from 'prop-types';
import { useTheme } from 'emotion-theming';
import { debounce, isFunction, isArray, isUndefined } from 'lodash';
import hash from 'object-hash';
import { Row, Col } from '../Grid';
import Dropdown from '../Dropdown';
import Icon from '../Icon';
import { Skeleton } from '../Loader';
import { useConstant } from '../hooks/useConstant';
import {
  removeNodes,
  prepareValue,
  prepareOptions,
  outputValue,
} from '../Dropdown/utils';
import { isStrNum, noValue, validate as inputValidation } from '../utils';
import {
  dynamicInputError,
  dynamicInputErrorAdd,
  dynamicInputContainer,
  dynamicInputLabel,
  dynamicInputAddLabel,
  dynamicInputValue,
  dynamicInputValueContainer,
  dynamicDropdownField,
  dynamicDropdownFieldContainer,
  requiredText,
  rightIconsContainer,
  dynamicInputEditIcon,
  dynamicDropdownClearIcon,
} from './styles';
import { compareDropdownProps } from './utils';

const DynamicDropdown = ({
  id,
  dropdownRef,
  label,
  value: initValueProp,
  required,
  onChange,
  onError,
  validate,
  mapValue,
  readOnly,
  className,
  isTouched: isTouchedInit,
  mapDisplayValue,
  noPrefilled,
  noSimplify,
  noClear,
  ...dropDownProps
}) => {
  const theme = useTheme();

  const {
    options: initOptions,
    multiSelect,
    disabled,
    displayKey = 'name',
    uniqueKey = 'code',
  } = dropDownProps;

  const options = prepareOptions(initOptions, displayKey);

  const preparedValue = prepareValue(
    initValueProp,
    options,
    multiSelect,
    displayKey,
    uniqueKey
  );

  const [isEdit, setIsEdit] = useState(false);
  const [error, setError] = useState(false);
  const [value, setValue] = useState(preparedValue);
  // Is touched is used to show error only after the first interaction
  // It is set to true only when the user click on the value to edit it
  const [isTouched, setIsTouched] = useState(isTouchedInit);
  const hasError = isTouched && !!error;

  // Wait until the animation finish and after that switch the view to read mode
  const [switchToReadMode] = useConstant(
    debounce(() => {
      setIsEdit(false);
      !isTouched && setIsTouched(true);
    }, 400)
  );

  const internalDropDownRef = useRef({});
  const isDisabled = disabled || !options?.length;

  useEffect(() => {
    setIsTouched(isTouchedInit);
  }, [isTouchedInit]);

  useEffect(() => {
    setValue(preparedValue);
  }, [hash({ val: removeNodes(preparedValue) })]);

  useEffect(() => {
    checkForError(value);
  }, [isTouched, hash({ value })]);

  // If there is only one element preselect it
  useEffect(() => {
    if (!noValue(value) || noPrefilled || !required) return;
    const optionsList =
      isArray(options) && options.filter(el => !el.groupLabel);
    if (optionsList?.length === 1) {
      handleSelect(multiSelect ? [optionsList[0]] : optionsList[0]);
      switchToReadMode();
    }
  }, [hash({ options: removeNodes(options) })]);

  useImperativeHandle(dropdownRef, () => ({
    changeValue: handleSelect,
    value: () => value,
    displayValue: () => displayValue,
    changeIsTouched: setIsTouched,
  }));

  const handleBlur = ({ currentTarget }) =>
    !isDisabled &&
    // This timeout is used to let one render to pass and after that to check which node is focused
    setTimeout(() => {
      if (currentTarget.contains(document.activeElement)) return;
      switchToReadMode();
    });

  const handleSelect = input => {
    if (disabled) return;

    const newValue = input ?? (multiSelect ? [] : null);
    setValue(newValue);
    onChange(id, outputValue(newValue, mapValue, !noSimplify));

    !multiSelect && switchToReadMode();
  };

  const handleClear = e => {
    e.stopPropagation();
    !isTouched && setIsTouched(true);
    handleSelect(null);
  };

  const checkForError = newValue => {
    let newError = null;

    // If validate function is provided check for error
    if (isFunction(validate)) newError = validate(newValue, id);
    else if (required) newError = inputValidation(newValue, 'required');
    onError(id, newError);
    isTouched && setError(newError);
  };

  const switchToEditView = () => {
    if (readOnly || isDisabled) return;
    setIsEdit(true);
  };

  const getOptionValue = val => (isStrNum(val) ? val : val[displayKey]);

  const getValue = () => {
    if (noValue(value)) return null;

    const isOff = readOnly || disabled;
    if (multiSelect) {
      const stringValue = value?.map(getOptionValue).join(', ');
      return stringValue?.length ? stringValue : isOff ? 'N/A' : null;
    }
    return getOptionValue(value) || (isOff ? 'N/A' : null);
  };

  const dropDownValue = getValue();

  const displayValue = isFunction(mapDisplayValue)
    ? mapDisplayValue(value)
    : dropDownValue;

  if (isUndefined(initValueProp) || (isUndefined(options) && !readOnly)) {
    return (
      <div className={dynamicInputLabel(false, false, theme)}>
        <Skeleton height={19} />
      </div>
    );
  }

  return (
    <div className={dynamicInputContainer(readOnly, className, theme)}>
      {!noValue(displayValue) || isEdit || readOnly || disabled ? (
        <Row lg={6}>
          <Col className={dynamicInputLabel(hasError, isDisabled, theme)}>
            {label}
            {required && <span className={requiredText}>&nbsp;(required)</span>}
            {hasError && (
              <p className={dynamicInputError(theme)}>{error.msg}</p>
            )}
          </Col>
          <Col>
            {isEdit ? (
              <div
                role="menuitem"
                tabIndex="0"
                onBlur={handleBlur}
                className={dynamicDropdownFieldContainer}>
                <Dropdown
                  ref={internalDropDownRef}
                  open
                  noClear={readOnly}
                  noSimplify
                  noPrefilled
                  autoWidth
                  alwaysFocused
                  value={value}
                  onChange={handleSelect}
                  className={dynamicDropdownField(hasError, theme)}
                  {...dropDownProps}
                  placeholder={
                    dropDownProps.placeholder || `Select ${label}...`
                  }
                />
              </div>
            ) : (
              <div
                className={dynamicInputValueContainer(
                  isDisabled,
                  readOnly,
                  theme
                )}
                role="button"
                tabIndex="0"
                onClick={switchToEditView}>
                <span
                  className={dynamicInputValue(
                    hasError,
                    isDisabled,
                    readOnly,
                    theme
                  )}>
                  {displayValue}
                </span>
                <div className={rightIconsContainer}>
                  {dropDownValue && !noClear && (
                    <Icon
                      iconName="close"
                      onClick={handleClear}
                      className={dynamicDropdownClearIcon}
                    />
                  )}
                  <Icon
                    iconName="edit"
                    color="primary"
                    className={dynamicInputEditIcon(theme)}
                  />
                </div>
              </div>
            )}
          </Col>
        </Row>
      ) : (
        <div
          role="button"
          tabIndex="-1"
          onClick={switchToEditView}
          className={dynamicInputAddLabel(
            hasError,
            isDisabled,
            required,
            theme
          )}>
          + Add {label}
          {required && <span className={requiredText}>&nbsp;(required)</span>}
          {hasError && (
            <p className={dynamicInputErrorAdd(theme)}>{error.msg}</p>
          )}
        </div>
      )}
    </div>
  );
};

DynamicDropdown.propTypes = {
  id: PropTypes.string.isRequired,
  dropdownRef: PropTypes.object,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.string,
    PropTypes.array,
  ]),
  required: PropTypes.bool,
  isTouched: PropTypes.bool,
  mapValue: PropTypes.func,
  mapDisplayValue: PropTypes.func,
  onChange: PropTypes.func,
  onError: PropTypes.func,
  validate: PropTypes.func,
  readOnly: PropTypes.bool,
  noPrefilled: PropTypes.bool,
  noSimplify: PropTypes.bool,
  noClear: PropTypes.bool,
  dropDownProps: PropTypes.object,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export default forwardRef((initProps, ref) =>
  useMemo(
    () => <DynamicDropdown {...initProps} dropdownRef={ref} />,
    compareDropdownProps(initProps)
  )
);

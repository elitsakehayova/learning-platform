import { useState, useEffect, useImperativeHandle } from 'react';
import { isFunction } from 'lodash';
import moment from 'moment';
import validateInput from '../utils/inputValidation';

const validateIsRequired = value => validateInput(value, 'required');

export const useFormInput = ({
  id,
  value: initValue,
  onChange: changeValueInForm,
  onError: changeErrorInForm,
  validate,
  pattern,
  disabled,
  readOnly,
  required,
  readOnlyEditable,
  isTouched: isTouchedInit,
  inputProps,
  inputRef,
}) => {
  const strVal = initValue?.toString ? initValue.toString() : initValue ?? '';
  const [value, setValue] = useState(strVal);
  const [error, setError] = useState();
  const [isTouched, setIsTouched] = useState(isTouchedInit);
  const isDateTimePicker = inputProps?.dateTime;
  const { format: formatDate } = inputProps?.dateTimeProps ?? {};

  useEffect(() => {
    setValue(strVal);
  }, [strVal]);

  useEffect(() => {
    checkForError(value);
  }, [isTouched]);

  useEffect(() => {
    setIsTouched(isTouchedInit);
  }, [isTouchedInit]);

  useEffect(() => {
    if (disabled || readOnly) return;
    // Form component holds all values and errors into state so we need to pass the new value and error to the form component
    checkForError(value);
    changeValueInForm(id, value);
  }, [value]);

  useImperativeHandle(inputRef, () => ({
    changeValue: newValue => onChange({ target: { value: newValue } }),
    checkForError,
    value,
    changeIsTouched: setIsTouched,
  }));

  // Handle change of the input,
  // All form inputs are controlled and have onChange
  const onChange = event => {
    const {
      target: { value: newValue },
    } = event;

    if (newValue.length && isFunction(pattern) && !pattern(newValue)) return;

    setValue(
      isDateTimePicker
        ? moment(newValue).format(formatDate ?? 'DD MMM YYYY')
        : newValue
    );
  };

  // On blur is used only to change the flag for touched
  const onBlur = () => {
    !isTouched && setIsTouched(true);
    checkForError(value);
  };

  const checkForError = newValue => {
    let newError = null;

    // If validate function is provided check for error
    if (isFunction(validate) && !newError) newError = validate(newValue, id);
    else if (required) newError = validateIsRequired(newValue);

    handleError(newError);
  };

  const handleError = newError => {
    if (error === newError) return;
    // The first time when you edit the field it will not generate error for the field
    // but it will pass the error to the form so you can not submit it until there is error
    isTouched && setError(newError);
    // The form component holds all errors into ref so we need to pass the new error into the form component
    changeErrorInForm(id, newError);
  };

  return {
    // Input props
    value,
    onChange,
    onBlur,
    disabled: readOnly || readOnlyEditable || disabled,
    // Form related props
    error,
    isTouched,
  };
};

import React, { forwardRef, useImperativeHandle } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { cx } from 'emotion';
import { useTheme } from 'emotion-theming';
import { Button } from '../Button';
import { useForm } from './useForm';
import BodyOverflow from '../BodyOverflow';
import { Spinner } from '../Loader';
import { formContainer, submitBar } from './styles';

const Form = forwardRef((props, ref) => {
  const {
    className,
    loaderClassName,
    renderBottomBar,
    children,
    submitButton = {
      text: 'Submit',
    },
  } = props;
  const theme = useTheme();

  const { loading, handleSubmit, newChildren, formOptions, reset } = useForm(
    props
  );

  const { resetKey, resetForm, reloadForm } = reset;

  useImperativeHandle(ref, () => ({
    ...formOptions,
    children,
    reset: resetForm,
    reload: reloadForm,
  }));

  const SubmitButton = (options = {}) => {
    const { text, secondary } = options;
    return (
      <Button secondary={secondary} onClick={handleSubmit} disabled={loading}>
        {text}
        <Spinner loading={loading} color={!secondary && 'secondary'} />
      </Button>
    );
  };

  const CancelButton = (options = {}) => {
    const { text, secondary } = options;
    return (
      <Button secondary={!secondary} onClick={resetForm} disabled={loading}>
        {text || 'Cancel'}
        <Spinner loading={loading} color={secondary && 'secondary'} />
      </Button>
    );
  };

  const BottomBar = renderBottomBar;

  return (
    <div
      key={resetKey}
      className={cx(className, formContainer(!!renderBottomBar))}>
      <Spinner
        loading={loading}
        className={cx(loaderClassName, formContainer(false))}
      />
      {newChildren}
      {renderBottomBar ? (
        <BodyOverflow>
          <div className={submitBar(!isEmpty(formOptions.values), theme)}>
            <BottomBar
              submit={SubmitButton}
              cancel={CancelButton}
              options={formOptions}
            />
          </div>
        </BodyOverflow>
      ) : (
        SubmitButton(submitButton, formOptions)
      )}
    </div>
  );
});

Form.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  loaderClassName: PropTypes.string,
  submitButton: PropTypes.object,
  renderBottomBar: PropTypes.func,
};

export default Form;

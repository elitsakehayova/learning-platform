import React, { useState, useMemo, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useTheme } from 'emotion-theming';
import Input from '../Input';
import Icon from '../Icon';
import { useFormInput } from './useFormInput';
import { compareInputProps } from './utils';
import {
  inputContainer,
  inputField,
  inputError,
  inputLabel,
  inputFieldRightIcon,
  inputFieldVisibilityIcon,
} from './styles';

const FormInput = props => {
  const { id, type = 'text', label, required, horizontal, placeholder } = props;
  const theme = useTheme();

  // We had a custom hook which handles the logic behind the input
  // onChange, onBlur are methods that are implemented into the hook
  const { isTouched, error, ...inputAttr } = useFormInput(props);

  const [visibility, setVisibility] = useState(false);
  const isPasswordType = type === 'password';

  const hasValue = !!(inputAttr.value && inputAttr.value.length);
  const isFilled = isTouched && hasValue;

  const ErrorRenderIcon = error?.render;
  const errorDefaultIcon = (
    <Icon
      iconName="error_outline"
      className={inputFieldRightIcon(isPasswordType)}
      color="errorDark"
    />
  );

  const errorMessage = (
    <p className={inputError(theme)}>{error?.msg || error}</p>
  );

  return (
    <div className={inputContainer(horizontal)}>
      <label htmlFor={id} className={inputLabel(horizontal, theme)}>
        {`${label}${required ? ' (Required)' : ''}`}
      </label>
      <Input
        type={visibility ? 'text' : type}
        className={inputField(!!error, hasValue, theme)}
        placeholder={placeholder || `${label}...`}
        {...inputAttr}
      />
      {isPasswordType && (
        <Icon
          iconName={visibility ? 'visibility_off' : 'visibility'}
          onClick={() => setVisibility(!visibility)}
          className={inputFieldVisibilityIcon}
          color="textLightPrimary"
        />
      )}
      {error &&
        (error?.render ? (
          <ErrorRenderIcon className={inputFieldRightIcon(isPasswordType)} />
        ) : (
          errorDefaultIcon
        ))}
      {error && errorMessage}
      {!error && isFilled && (
        <Icon
          iconName="done"
          className={inputFieldRightIcon(isPasswordType)}
          color="primary"
        />
      )}
    </div>
  );
};

FormInput.propTypes = {
  inputRef: PropTypes.object,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onError: PropTypes.func,
  placeholder: PropTypes.string,
  validate: PropTypes.func,
  pattern: PropTypes.func,
  horizontal: PropTypes.bool,
};

export default forwardRef((initProps, ref) =>
  useMemo(
    () => <FormInput {...initProps} inputRef={ref} />,
    compareInputProps(initProps)
  )
);

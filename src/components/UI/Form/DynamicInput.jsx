import React, { useState, useRef, useMemo, useEffect, forwardRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { isNil, isUndefined } from 'lodash';
import { useTheme } from 'emotion-theming';
import { Row, Col } from '../Grid';
import Input from '../Input';
import Icon from '../Icon';
import { Skeleton } from '../Loader';
import { useFormInput } from './useFormInput';
import { compareInputProps } from './utils';
import {
  dynamicInputContainer,
  dynamicInputLabel,
  dynamicInputAddLabel,
  dynamicInputValue,
  dynamicInputError,
  dynamicInputErrorAdd,
  dynamicInputField,
  dynamicInputValueContainer,
  requiredText,
  dynamicInputEditIcon,
} from './styles';

const DynamicInput = props => {
  const {
    id,
    label,
    value: initValueProp,
    type = 'text',
    inputProps,
    placeholder,
    readOnly: initReadOnly,
    readOnlyEditable,
    required,
    disabled,
    className,
  } = props;

  const theme = useTheme();
  const [isEdit, setIsEdit] = useState(false);
  const internalInputRef = useRef(null);
  const { error, onBlur, onChange, value } = useFormInput(props);
  const hasValue = !isNil(value) && value.length;
  const readOnly = initReadOnly || readOnlyEditable;
  const isDateTimePicker = inputProps?.dateTime;
  const { format: formatDate } = inputProps?.dateTimeProps ?? {};

  useEffect(() => {
    isEdit && internalInputRef.current?.focus();
  });

  const handleBlur = e => {
    if (disabled || readOnly) return;
    setIsEdit(false);
    onBlur(e);
  };

  const switchToEditView = () => !readOnly && !disabled && setIsEdit(true);

  if (isUndefined(initValueProp)) {
    return (
      <div className={dynamicInputLabel(false, false, theme)}>
        <Skeleton height={19} />
      </div>
    );
  }

  const displayValue = isDateTimePicker
    ? moment(value).format(formatDate ?? 'DD MMM YYYY')
    : value;

  return (
    <div className={dynamicInputContainer(readOnly, className, theme)}>
      {hasValue || isEdit || readOnly ? (
        <Row lg={6}>
          <Col className={dynamicInputLabel(error, disabled, theme)}>
            {label}
            {required && <span className={requiredText}>&nbsp;(required)</span>}
            {error && <p className={dynamicInputError(theme)}>{error.msg}</p>}
          </Col>
          <Col>
            {isEdit ? (
              <div>
                <Input
                  id={id}
                  ref={internalInputRef}
                  type={type}
                  onBlur={handleBlur}
                  onChange={onChange}
                  value={value}
                  placeholder={placeholder || `${label}...`}
                  className={dynamicInputField(error, theme)}
                  {...inputProps}
                />
              </div>
            ) : (
              <div
                className={dynamicInputValueContainer(
                  disabled,
                  readOnly,
                  theme
                )}
                role="button"
                tabIndex="-2"
                onClick={switchToEditView}>
                <span
                  className={dynamicInputValue(
                    error,
                    disabled,
                    readOnly,
                    theme
                  )}>
                  {displayValue?.length ? displayValue : 'N/A'}
                </span>
                {!readOnly && (
                  <Icon
                    iconName="edit"
                    color="primary"
                    className={dynamicInputEditIcon(theme)}
                  />
                )}
              </div>
            )}
          </Col>
        </Row>
      ) : (
        <div
          role="button"
          tabIndex="-1"
          onClick={switchToEditView}
          className={dynamicInputAddLabel(error, disabled, required, theme)}>
          + Add {label}
          {required && <span className={requiredText}>&nbsp;(required)</span>}
          {error && <p className={dynamicInputErrorAdd(theme)}>{error.msg}</p>}
        </div>
      )}
    </div>
  );
};

DynamicInput.propTypes = {
  id: PropTypes.string.isRequired,
  inputRef: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
  onError: PropTypes.func,
  validate: PropTypes.func,
  readOnly: PropTypes.bool,
  readOnlyEditable: PropTypes.bool,
  placeholder: PropTypes.string,
  inputProps: PropTypes.object,
  pattern: PropTypes.func,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  type: PropTypes.string,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export default forwardRef((refProps, ref) =>
  useMemo(
    () => <DynamicInput {...refProps} inputRef={ref} />,
    compareInputProps(refProps)
  )
);

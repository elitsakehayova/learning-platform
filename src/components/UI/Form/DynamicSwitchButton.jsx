import React, {
  useRef,
  useState,
  useEffect,
  useMemo,
  forwardRef,
  useImperativeHandle,
} from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'emotion-theming';
import { isNil, isBoolean, debounce, isUndefined } from 'lodash';
import hash from 'object-hash';
import { Row, Col } from '../Grid';
import { SwitchButton } from '../Button';
import Icon from '../Icon';
import { Skeleton } from '../Loader';
import { useConstant } from '../hooks/useConstant';
import {
  dynamicInputContainer,
  dynamicInputLabel,
  dynamicInputAddLabel,
  dynamicInputValue,
  dynamicInputValueContainer,
  dynamicSwitchButtonFieldContainer,
  dynamicInputEditIcon,
} from './styles';

const DynamicSwitchButton = ({
  id,
  switchButtonRef,
  label,
  value: initValue,
  onChange,
  theme,
  mapValue,
  readOnly: initReadOnly,
  readOnlyEditable,
  disabled,
  width,
  activeText,
  inactiveText,
  className,
  style,
}) => {
  const containerRef = useRef();
  const [isEdit, setIsEdit] = useState(false);
  const [value, setValue] = useState(initValue);
  const [switchToReadMode] = useConstant(
    debounce(() => setIsEdit(false), 1000)
  );
  const readOnly = initReadOnly || readOnlyEditable;

  useEffect(() => {
    setValue(initValue);
  }, [hash({ initValue })]);

  useEffect(() => {
    isEdit && containerRef.current?.focus();
  }, [isEdit]);

  useImperativeHandle(switchButtonRef, () => ({
    changeValue: newValue => setValue(!!newValue),
  }));

  const handleSelect = val => {
    if (disabled || initReadOnly) return;
    const mappedValue = mapValue ? mapValue(val) : val;
    setValue(val);
    onChange(id, mappedValue);
    switchToReadMode();
  };

  const switchToEditView = () => !readOnly && !disabled && setIsEdit(true);

  if (isUndefined(initValue)) {
    return (
      <div className={dynamicInputLabel(false, false, theme)}>
        <Skeleton height={19} />
      </div>
    );
  }

  return (
    <div className={dynamicInputContainer(readOnly, className, theme)}>
      {isBoolean(value) || isEdit || readOnly ? (
        <Row lg={6}>
          <Col className={dynamicInputLabel(false, disabled, theme)}>
            {label}
          </Col>
          <Col>
            {isEdit ? (
              <div
                ref={containerRef}
                tabIndex="-1"
                onBlur={() => setIsEdit(false)}
                className={dynamicSwitchButtonFieldContainer}>
                <SwitchButton
                  id={id}
                  value={!!value}
                  onChange={handleSelect}
                  activeText={activeText}
                  inactiveText={inactiveText}
                  width={width}
                  style={style}
                />
              </div>
            ) : (
              <div
                className={dynamicInputValueContainer(
                  disabled,
                  readOnly,
                  theme
                )}
                role="button"
                tabIndex="0"
                onClick={switchToEditView}>
                <span
                  className={dynamicInputValue(
                    false,
                    disabled,
                    readOnly,
                    theme
                  )}>
                  {isNil(value)
                    ? 'N/A'
                    : value
                    ? activeText || 'On'
                    : inactiveText || 'Off'}
                </span>
                <Icon
                  iconName="edit"
                  color="primary"
                  className={dynamicInputEditIcon(theme)}
                />
              </div>
            )}
          </Col>
        </Row>
      ) : (
        <div
          role="button"
          tabIndex="-1"
          onClick={switchToEditView}
          className={dynamicInputAddLabel(false, disabled, false, theme)}>
          + Add {label}
        </div>
      )}
    </div>
  );
};

DynamicSwitchButton.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  switchButtonRef: PropTypes.object,
  value: PropTypes.bool,
  mapValue: PropTypes.func,
  onChange: PropTypes.func,
  theme: PropTypes.object,
  readOnly: PropTypes.bool,
  readOnlyEditable: PropTypes.bool,
  disabled: PropTypes.object,
  width: PropTypes.number,
  activeText: PropTypes.string,
  inactiveText: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

export default forwardRef((initProps, ref) => {
  const { disabled, className, readOnly, value } = initProps;

  return useMemo(
    () =>
      withTheme(DynamicSwitchButton).render({
        ...initProps,
        switchButtonRef: ref,
      }),
    [disabled, className, readOnly, value]
  );
});

import { css, cx, keyframes } from 'emotion';

// Form styles start
export const formContainer = hasBottomBar =>
  css(
    {
      width: '100%',
    },
    hasBottomBar && { paddingBottom: 60 }
  );

export const submitBar = (isShown, theme) =>
  css({
    position: 'fixed',
    bottom: isShown ? 0 : '-100%',
    left: 0,
    right: 0,
    boxShadow: '0 -2px 4px 0 rgba(0,0,0,0.2)',
    backgroundColor: 'white',
    padding: 16,
    color: theme.textLightPrimary,
    transition: 'bottom 500ms ease-in-out',
  });
// Form styles end

// Input styles start (base one)
export const inputContainer = isHorizontal =>
  css({
    display: 'flex',
    flexDirection: isHorizontal ? 'row' : 'column',
    alignItems: 'flex-start',
    justifyContent: isHorizontal ? 'space-between' : 'flex-start',
    position: 'relative',
    width: '100%',
    height: isHorizontal ? '54px' : '86px',
    marginBottom: '16px',
    outline: 'none',
  });

export const horizontalContainer = css({
  flex: 1,
  position: 'relative',
});

export const inputLabel = (isHorizontal, theme, isDisabled) =>
  css({
    color: theme.textLightSecondary,
    fontSize: '14px',
    lineHeight: '20px',
    opacity: isDisabled ? 0.3 : 1,
    margin: `
      ${isHorizontal ? '12px' : 0}
      ${isHorizontal ? '24px' : 0}
      ${isHorizontal ? 0 : '8px'}
      0`,
  });

export const inputField = (hasError, hasText, theme) =>
  css(
    hasError
      ? {
          border: `1px solid ${theme.errorDark}`,
        }
      : hasText
      ? {
          border: `1px solid ${theme.primary}`,
        }
      : {}
  );

export const inputError = theme =>
  css({
    fontSize: '12px',
    lineHeight: 1.5,
    color: theme.errorDark,
  });

export const inputErrorAdd = theme =>
  css(inputError(theme), {
    paddingLeft: 12,
  });

export const inputFieldRightIcon = isPasswordType =>
  css({
    position: 'absolute',
    right: isPasswordType ? 38 : 8,
    bottom: 26,
  });

export const inputFieldVisibilityIcon = css({
  position: 'absolute',
  right: 8,
  bottom: 26,
});
// Input styles end (base one)

// Input styles start (dynamic one)
export const dynamicInputContainer = (isReadOnly, className, theme) =>
  cx(
    css({
      borderRadius: 8,
      paddingLeft: 8,
      '&:hover': {
        backgroundColor: isReadOnly ? 'transparent' : theme.whiteGray,
      },
      '@media (max-width: 991px)': {
        marginTop: 8,
      },
    }),
    className
  );

export const dynamicInputError = theme =>
  css({
    fontSize: '12px',
    lineHeight: 1,
    color: theme.errorDark,
  });

export const dynamicInputErrorAdd = theme =>
  css(dynamicInputError(theme), {
    paddingLeft: 12,
  });

export const dynamicInputAddLabel = (hasError, isDisabled, required, theme) =>
  css({
    color: hasError
      ? theme.errorDark
      : isDisabled
      ? theme.textLightDisabled
      : theme.primary,
    fontSize: 16,
    lineHeight: hasError ? '17px' : '21px',
    padding: required
      ? hasError
        ? '5px 0'
        : '8px 0 10px'
      : hasError
      ? '8px 0 0'
      : '8px 0 12px',
    outline: 'none',
    cursor: isDisabled ? 'not-allowed' : 'pointer',
  });

export const dynamicInputLabel = (hasError, isDisabled, theme) =>
  css({
    color: hasError
      ? theme.errorDark
      : isDisabled
      ? theme.textLightDisabled
      : theme.textLightSecondary,
    padding: hasError ? '5px 0 5px 14px' : '10px 0 12px 14px',
    '@media (max-width: 991px)': {
      padding: '10px 0',
    },
  });

export const dynamicInputValue = (hasError, isDisabled, isReadOnly, theme) =>
  css({
    color: hasError
      ? theme.errorDark
      : isDisabled
      ? theme.textLightDisabled
      : isReadOnly
      ? theme.textLightSecondary
      : theme.textLightPrimary,
    display: 'flex',
    alignItems: 'center',
    padding: '11px 9px',
    '@media (max-width: 991px)': {
      padding: '11px 9px 11px 0',
    },
  });

export const dynamicInputEditIcon = theme =>
  css({
    opacity: 0,
    fontSize: 20,
    padding: 3,
    borderRadius: 6,
    marginRight: 6,
    border: `1px solid ${theme.primary}`,
  });

export const dynamicDropdownClearIcon = css({
  opacity: 0,
  fontSize: 20,
  padding: 3,
  marginRight: 6,
});

export const dynamicInputValueContainer = (isDisabled, isReadOnly) =>
  css({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    cursor: isReadOnly ? 'default' : isDisabled ? 'not-allowed' : 'pointer',
    outline: 'none',

    '&:hover i':
      !isDisabled && !isReadOnly
        ? {
            opacity: 1,
          }
        : {},
  });

export const dynamicInputField = (hasError, theme) =>
  css({
    borderColor: hasError ? theme.errorDark : theme.primary,
  });
// Input styles end (dynamic one)

// Dropdown styles start
export const dropdownInput = (hasError, theme) =>
  hasError ? css({ borderColor: theme.errorDark }) : '';
// DropDown styles end

// Dynamic dropdown styles start
export const dynamicDropdownField = (hasError, theme) =>
  css(
    {
      paddingLeft: 9,
    },
    hasError
      ? {
          borderColor: theme.errorDark,
        }
      : {}
  );

export const dynamicDropdownFieldContainer = css({
  outline: 'none',
});

export const requiredText = css({
  fontSize: '11px',
  textTransform: 'capitalize',
});

export const dynamicSwitchButtonFieldContainer = css({
  outline: 'none',
  paddingTop: 6,
});

export const rightIconsContainer = css({
  display: 'flex',
});

// Dynamic dropdown styles end

// Success Bar styles start
const successBarEffect = keyframes`
  from {
    width: 0;
  }
  to {
    width: 100%;
  }
`;

export const successBar = css({
  position: 'fixed',
  bottom: 0,
  left: 0,
  right: 0,
  boxShadow: '0 -2px 4px 0 rgba(0,0,0,0.2)',
  backgroundColor: 'white',
  height: 70,
  overflow: 'hidden',
  textAlign: 'center',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  color: 'white',
  fontSize: 18,
});

export const successBarColorEffect = theme =>
  css({
    width: '100%',
    position: 'absolute',
    top: 0,
    bottom: 0,
    backgroundColor: theme.successDark,
    animation: `${successBarEffect} 0.7s ease-in-out`,
  });

export const successBarContent = css({
  position: 'relative',
  zIndex: 10,
  width: '100%',

  '& > i': {
    position: 'absolute',
    right: '20px',
    top: '0',
    cursor: 'pointer',
  },
});
// Success Bar styles end

import React, {
  useState,
  useRef,
  useEffect,
  useMemo,
  forwardRef,
  useImperativeHandle,
} from 'react';
import PropTypes from 'prop-types';
import { useTheme } from 'emotion-theming';
import { isFunction } from 'lodash';
import Dropdown from '../Dropdown';
import { outputValue } from '../Dropdown/utils';
import { compareDropdownProps } from './utils';
import { validate as inputValidation } from '../utils';
import {
  inputContainer,
  inputLabel,
  inputError,
  dropdownInput,
  horizontalContainer,
} from './styles';

const DropdownForm = ({
  dropdownRef,
  id,
  required,
  onChange,
  onError,
  label,
  validate,
  horizontal,
  value,
  mapValue,
  noSimplify,
  isTouched: isTouchedInit,
  ...dropDownProps
}) => {
  const theme = useTheme();
  const { disabled, options, multiSelect } = dropDownProps;
  const [error, setError] = useState(false);
  const internalDropDownRef = useRef(null);
  const [isTouched, setIsTouched] = useState(isTouchedInit);
  const hasError = isTouched && error;
  const isDisabled = disabled || !options?.length;

  useImperativeHandle(dropdownRef, () => ({
    changeValue: newValue => {
      handleSelect(newValue);
      internalDropDownRef.current?.changeValue(newValue);
    },
    value: () => internalDropDownRef.current?.value,
    displayValue: () => internalDropDownRef.current?.displayValue,
    changeIsTouched: setIsTouched,
  }));

  useEffect(() => {
    setIsTouched(isTouchedInit);
  }, [isTouchedInit]);

  useEffect(() => {
    checkForError(internalDropDownRef.current.value);
  }, [isTouched]);

  const handleSelect = input => {
    const newValue = input ?? (multiSelect ? [] : null);
    checkForError(newValue);
    onChange(id, outputValue(newValue, mapValue, !noSimplify));
  };

  const handleBlur = ({ currentTarget }) =>
    !isDisabled &&
    setTimeout(() => {
      if (currentTarget.contains(document.activeElement)) return;
      isTouched
        ? checkForError(internalDropDownRef.current.value)
        : setIsTouched(true);
    });

  const checkForError = newValue => {
    let newError = null;

    // If validate function is provided check for error
    if (isFunction(validate)) newError = validate(newValue, id);
    else if (required) newError = inputValidation(newValue, 'required');
    onError(id, newError);
    isTouched && setError(newError);
  };

  const dropdownComponent = (
    <>
      <Dropdown
        ref={internalDropDownRef}
        value={value}
        autoWidth
        noSimplify
        noPrefilled={!required}
        className={dropdownInput(hasError, theme)}
        onChange={handleSelect}
        {...dropDownProps}
        placeholder={dropDownProps.placeholder || `Select ${label}...`}
      />
      {hasError && <p className={inputError(theme)}>{error?.msg}</p>}
    </>
  );

  return (
    <div
      role="menuitem"
      tabIndex="0"
      onBlur={handleBlur}
      className={inputContainer(horizontal)}>
      <label htmlFor={id} className={inputLabel(horizontal, theme, isDisabled)}>
        {`${label} ${required ? ' (Required)' : ''}`}
      </label>
      {horizontal ? (
        <div className={horizontalContainer}>{dropdownComponent}</div>
      ) : (
        dropdownComponent
      )}
    </div>
  );
};

DropdownForm.propTypes = {
  dropdownRef: PropTypes.object,
  id: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.string,
    PropTypes.number,
  ]),
  required: PropTypes.bool,
  onError: PropTypes.func,
  onChange: PropTypes.func,
  mapValue: PropTypes.func,
  horizontal: PropTypes.bool,
  validate: PropTypes.func,
  noSimplify: PropTypes.bool,
  isTouched: PropTypes.bool,
  label: PropTypes.string,
};

export default forwardRef((initProps, ref) =>
  useMemo(
    () => <DropdownForm {...initProps} dropdownRef={ref} />,
    compareDropdownProps(initProps)
  )
);

import { css } from 'emotion';

export const navBarContainer = theme =>
  css({
    color: theme.primary,
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 60,
    position: 'fixed',
    zIndex: 100,
    top: 0,
    backgroundColor: 'white',
    borderBottom: `1px solid ${theme.borderLight}`,
    padding: '0 14px',
  });

export const leftSideContainer = css({
  display: 'flex',
  flexFlow: 'row nowrap',
  alignItems: 'center',
});

export const rightSideContainer = css(leftSideContainer, {
  '& > *': {
    marginRight: 5,
    cursor: 'pointer',
    '&:last-child': {
      marginRight: 0,
    },
  },
});

export const usernameBranchContainer = css(leftSideContainer, {
  marginLeft: 4,
});

export const appAvatarIcon = css({
  height: 36,
  margin: '0 5px 0 0',
});

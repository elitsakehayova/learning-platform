import React from 'react';
import PropTypes from 'prop-types';
import { useTheme } from 'emotion-theming';
import { Link } from 'react-router-dom';
import logo from '../../../assets/images/logo.svg';
import {
  navBarContainer,
  leftSideContainer,
  rightSideContainer,
  usernameBranchContainer,
  appAvatarIcon,
} from './styles';

const NavBar = props => {
  const { homeRoute } = props;
  const theme = useTheme();

  return (
    <header className={navBarContainer(theme)}>
      <div className={leftSideContainer}>
        <Link to={homeRoute} className={usernameBranchContainer}>
          <img src={logo} className={appAvatarIcon} alt="Logo" />
          Left Side Nav
        </Link>
      </div>
      <div className={rightSideContainer}>Right side nav</div>
    </header>
  );
};

NavBar.propTypes = {
  homeRoute: PropTypes.string,
};

export default NavBar;

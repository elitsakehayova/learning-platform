/* eslint-disable prefer-promise-reject-errors */
import axios from 'axios';
import * as jwt from 'jsonwebtoken';

// This is the list of waiting requests that will retry after the JWT refresh complete
let requestsQueue = [];
let isAlreadyFetchingAccessToken = false;

const setUpAuthHeader = token => {
  axios.defaults.headers.common.Authorization = `Bearer ${token}`;
};

// Proceed to the token refresh procedure
// We create a new Promise that will retry the request,
// clone all the request configuration from the failed request in the error object.
const queueRequest = initConfig =>
  new Promise(resolve => {
    // We need to add the request retry to the queue
    // since there another request that already attempt to refresh the token
    requestsQueue.push(accessToken => {
      const config = { ...initConfig };
      config.headers.Authorization = `Bearer ${accessToken}`;
      resolve(axios(config));
    });
  });

const refreshToken = async auth0Service => {
  // Try to refresh the token if there is no attempt in progress
  if (isAlreadyFetchingAccessToken) return;
  try {
    isAlreadyFetchingAccessToken = true;
    const token = await auth0Service.getTokenSilently();

    // After success of refreshing the token set it up to default headers
    // and empty the list of waiting promises to be resolved and clean the list
    setUpAuthHeader(token);
    requestsQueue.forEach(callback => callback(token));
    requestsQueue = [];
    isAlreadyFetchingAccessToken = false;
  } catch (err) {
    // If there is error with the generating of a new token (the user is blocked or something else)
    // we need to logout the user and he can try to login again
    auth0Service.logout({
      returnTo: window.location.origin,
    });
  }
};

const handle401 = (auth0Service, response) => {
  const retryOriginalRequest = queueRequest(response.config);
  refreshToken(auth0Service);

  // return the promise so the controller will not throw exception and will wait
  return retryOriginalRequest;
};

const checkRequest = config => {
  if (isAlreadyFetchingAccessToken) return queueRequest(config);
  return config;
};

export const initUnauthInterceptor = (auth0Service, initToken) => {
  setUpAuthHeader(initToken);
  // Start timeout to refresh the token after exp time ends
  const decodedToken = jwt.decode(initToken);

  setTimeout(async () => {
    refreshToken(auth0Service);
  }, decodedToken.exp * 1000 - new Date().getTime() + 1000);

  axios.interceptors.request.use(
    config => checkRequest(config),
    error => Promise.reject(error)
  );

  axios.interceptors.response.use(
    res => Promise.resolve(res),
    async ({ response = {} }) => {
      if (response.status === 401) return handle401(auth0Service, response);

      if (response.status === 403) {
        return Promise.reject({
          data: {
            errors: [
              {
                text: 'You are unauthorized.',
                codes: ['403'],
              },
            ],
          },
        });
      }
      return Promise.reject(response);
    }
  );
};

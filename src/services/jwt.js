/* eslint-disable camelcase */
import * as jwt from 'jsonwebtoken';
import axios from 'axios';
import { isUndefined, isNumber } from 'lodash';
import { initUnauthInterceptor } from './axios/interceptors';

const addUserClaims = (encodedToken, userData) => {
  const token = jwt.decode(encodedToken);
  const newUserData = userData;
  newUserData.claims = {};

  // Iterate through all keys inside the decoded token and save the one that are claims related
  Object.keys(token).forEach(tokenKey => {
    if (tokenKey.indexOf('claims') !== -1) {
      if (isUndefined(axios.defaults.claims)) axios.defaults.claims = {};
      const claimName = tokenKey.split('/').pop();
      if (claimName === 'partner_group_code') {
        const {
          [tokenKey]: [groupCode],
        } = token;
        token[tokenKey] = groupCode;
      }
      newUserData.claims[claimName] = token[tokenKey];
      axios.defaults.claims[claimName] = token[tokenKey];
    }
  });

  const isTenant = newUserData.claims.scope?.some(el => el === 'tenant_group');

  axios.defaults.isTenant = isTenant;
  newUserData.isTenant = isTenant;
  const hrSystemIndex = newUserData.claims?.system_name?.indexOf('hr');
  newUserData.isAuthorized = isNumber(hrSystemIndex) && hrSystemIndex !== -1;
  newUserData.userId = token.sub;

  return newUserData;
};

export const setUpUser = (auth0FromHook, token, userData, setUser) => {
  const newUserData = addUserClaims(token, userData);

  // Prepare interceptor for token renew
  initUnauthInterceptor(auth0FromHook, token);
  setUser(newUserData);
};

/* eslint-disable no-restricted-syntax */
/* eslint-disable max-len */
import { flattenDeep, isEmpty, isFunction } from 'lodash';
import { isStrNum } from 'elboo';

const validators = {
  required: {
    regExp: /\S/,
    msg: 'This field is required',
  },
  email: {
    regExp: /^$|^(([^<>()\\.,;:\s@"]+(\.[^<>()\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    msg: 'Invalid Email',
  },
  username: {
    regExp: /^$|^[A-Za-z0-9_+\-.!#$'^`~@]{1,25}$/,
    msg: 'Check error icon for info',
  },
  password: {
    regExp: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%^&*]).{8,}$/,
    msg: 'Check error icon for info',
  },
  phone: {
    regExp: /^$|^[6-9][0-9]{9}$/,
    msg: 'Invalid phone number',
  },
  postCode: {
    regExp: /^$|^[0-9]{6}$/,
    msg: 'Invalid post code',
  },
};

export default (initValue, ...inputValidators) => {
  const setOfValidators = flattenDeep(inputValidators);
  const value = initValue ?? '';

  for (const validator of setOfValidators) {
    const validatorName = validator?.name ?? validator;
    const { regExp, msg, render } = validators[validatorName];

    // For objects or arrays coming from dropdown we have only validation for required
    // Which means it can not be empty, for string or number test it with regexp
    const error = isStrNum(value) ? !regExp.test(value) : isEmpty(value);

    if (!error) continue;

    return {
      msg,
      ...(isFunction(render) && { render }),
    };
  }
  return null;
};

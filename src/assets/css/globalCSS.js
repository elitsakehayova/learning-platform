import { injectGlobal } from 'emotion';
import MaterialIconsRegularEot from '../fonts/MaterialIcons-Regular.eot';
import MaterialIconsRegularWoff2 from '../fonts/MaterialIcons-Regular.woff2';
import MaterialIconsRegularWoff from '../fonts/MaterialIcons-Regular.woff';
import MaterialIconsRegularTtf from '../fonts/MaterialIcons-Regular.ttf';

injectGlobal`
@import url('https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap');
    
@font-face {
  font-family: 'Material Icons';
  font-style: normal;
  src: url(${MaterialIconsRegularEot}); /* For IE6-8 */
  src: local('Material Icons'),
       local('MaterialIcons-Regular'),
       url(${MaterialIconsRegularWoff2}) format('woff2'),
       url(${MaterialIconsRegularWoff}) format('woff'),
       url(${MaterialIconsRegularTtf}) format('truetype');
}
html {
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
  margin-right: calc(100% - 100vw);
}
body {
  font-family: "Roboto", sans-serif;
  font-weight: 300;
  color: rgba(0, 0, 0, 0.87);
  overflow-x: hidden;
  margin: 0;
}

article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}
audio,canvas,progress,video{display:inline-block;vertical-align:baseline}
audio:not([controls]){display:none;height:0}
[hidden],.hidden,template{display:none}
p,figure,h1,h2,h3,h4,h5,h6,button,input,optgroup,select,textarea{margin:0}
a{background-color:transparent}
a:active,a:hover{outline:0}
a{color:inherit;text-decoration:none}
a:focus,a:hover{text-decoration:none}
a:focus{outline:0}
abbr[title]{border-bottom:1px dotted}
b,strong{font-weight:700}
dfn{font-style:italic}
h1{font-size:2em;}
h2{font-size:1.625em;}
h3{font-size:1.375em;}
h4{font-size:1.125em;}
h5{font-size:1em;}
h6{font-size:.9em;}
mark{background:#ff0;color:#000}
small{font-size:80%}
sub,sup{font-size:75%;line-height:0;e;vertical-align:baseline}
sup{top:-.5em}
sub{bottom:-.25em}
img{border:0}
svg:not(:root){overflow:hidden}
hr{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;height:0}
pre{overflow:auto}
code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}
button,input,optgroup,select,textarea{color:inherit;font:inherit;}
button{overflow:visible}
button,select{text-transform:none}
button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer}
button[disabled],html input[disabled]{cursor:not-allowed}
button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}
input{line-height:normal}
input[type=checkbox],input[type=radio]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0}
input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{height:auto}
input[type=search]{-webkit-appearance:textfield;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}
input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}
fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}
legend{border:0;padding:0}
textarea{overflow:auto}
optgroup{font-weight:700}
table{border-collapse:collapse;border-spacing:0}
td,th{padding:0}
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}
:after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}
button,input,select,textarea{font-family:inherit;font-size:inherit;line-height:inherit}
img{vertical-align:middle}
.img-responsive{display: block;max-width:100%;height:auto;}
hr{border:0;border-top:1px solid #eee}
[role=button]{cursor:pointer;outline: none;}`;

export default theme => injectGlobal`
  color: ${theme.textLightPrimary};
`;

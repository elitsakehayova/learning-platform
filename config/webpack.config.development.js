const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebappWebpackPlugin = require('webapp-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const ModuleNotFoundPlugin = require('react-dev-utils/ModuleNotFoundPlugin');
const paths = require('./paths');

module.exports = () => ({
  mode: 'development',
  bail: false,
  devtool: 'cheap-module-source-map',
  entry: [
    require.resolve('react-dev-utils/webpackHotDevClient'),
    paths.appIndexJs,
  ].filter(Boolean),
  output: {
    path: undefined,
    pathinfo: true,
    filename: 'js/bundle.js',
    futureEmitAssets: true,
    chunkFilename: 'js/[name].chunk.js',
    publicPath: '/',
    devtoolModuleFilenameTemplate: info =>
      path.resolve(info.absoluteResourcePath).replace(/\\/g, '/'),
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      name: false,
    },
    runtimeChunk: true,
  },
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
    modules: ['node_modules', paths.appNodeModules],
    extensions: paths.moduleFileExtensions.map(ext => `.${ext}`),
    plugins: [new ModuleScopePlugin(paths.appSrc, [paths.appPackageJson])],
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'eslint-loader',
        include: paths.appSrc,
        enforce: 'pre',
        options: {
          failOnError: true,
          emitWarning: true,
        },
      },
      {
        test: /\.(js|jsx)$/,
        enforce: 'pre',
        use: ['source-map-loader'],
      },
      {
        test: /\.(js|jsx)$/,
        include: paths.appSrc,
        loader: 'babel-loader',
        options: {
          customize: require.resolve(
            'babel-preset-react-app/webpack-overrides'
          ),
          cacheDirectory: true,
          cacheCompression: false,
          compact: false,
          sourceMaps: true,
        },
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        issuer: {
          test: /\.jsx?$/,
        },
        loader: '@svgr/webpack',
      },
      {
        oneOf: [
          // Include all assets
          {
            test: [/\.(bmp|gif|jpe?g|png)$/],
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'assets/images/[name].[ext]',
            },
          },
          {
            test: [/\.(eot|ttf|woff2?)$/],
            loader: 'file-loader',
            options: {
              name: 'assets/fonts/[name].[ext]',
            },
          },
          // Fallback for all other files (config, fonts and so on)
          {
            loader: 'file-loader',
            exclude: [/\.(js|jsx)$/, /\.html$/, /\.json$/],
            options: {
              name: 'assets/[name].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: paths.appHtml,
    }),
    new ModuleNotFoundPlugin(paths.appPath),
    new webpack.DefinePlugin({
      env: JSON.stringify('development'),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new WatchMissingNodeModulesPlugin(paths.appNodeModules),
    new WebappWebpackPlugin({
      logo: './src/assets/images/logo.svg',
      prefix: 'assets/icons/',
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
  node: {
    module: 'empty',
    dgram: 'empty',
    dns: 'mock',
    fs: 'empty',
    http2: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  },
  performance: false,
});

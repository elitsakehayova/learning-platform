const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InlineChunkHtmlPlugin = require('react-dev-utils/InlineChunkHtmlPlugin');
const TerserPlugin = require('terser-webpack-plugin');
const WebappWebpackPlugin = require('webapp-webpack-plugin');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const ModuleNotFoundPlugin = require('react-dev-utils/ModuleNotFoundPlugin');
const paths = require('./paths');

module.exports = env => {
  const isQAEnv = env === 'qa';

  return {
    mode: 'production',
    bail: true,
    devtool: isQAEnv ? 'source-map' : false,
    entry: paths.appIndexJs,
    output: {
      path: paths.appBuild,
      pathinfo: false,
      filename: 'js/[name].[contenthash:8].js',
      futureEmitAssets: true,
      chunkFilename: 'js/[name].[contenthash:8].chunk.js',
      publicPath: '/',
      devtoolModuleFilenameTemplate: info =>
        path
          .relative(paths.appSrc, info.absoluteResourcePath)
          .replace(/\\/g, '/'),
    },
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            parse: { ecma: 8 },
            compress: {
              ecma: 5,
              warnings: false,
              comparisons: false,
              inline: 2,
            },
            mangle: {
              safari10: true,
            },
            output: {
              ecma: 5,
              comments: false,
              ascii_only: true,
            },
          },
          parallel: true,
          cache: true,
          sourceMap: true,
        }),
      ],
      splitChunks: {
        chunks: 'all',
        name: false,
      },
      runtimeChunk: true,
    },
    resolve: {
      modules: ['node_modules', paths.appNodeModules],
      extensions: paths.moduleFileExtensions.map(ext => `.${ext}`),
      plugins: [new ModuleScopePlugin(paths.appSrc, [paths.appPackageJson])],
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          test: /\.(js|jsx)$/,
          loader: 'eslint-loader',
          include: paths.appSrc,
          enforce: 'pre',
          options: {
            emitError: true,
            failOnError: true,
          },
        },
        {
          test: /\.(js|jsx)$/,
          include: paths.appSrc,
          loader: 'babel-loader',
          options: {
            customize: require.resolve(
              'babel-preset-react-app/webpack-overrides'
            ),
            cacheDirectory: true,
            cacheCompression: true,
            compact: true,
          },
        },
        {
          test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
          issuer: {
            test: /\.jsx?$/,
          },
          loader: '@svgr/webpack',
        },
        {
          oneOf: [
            // Include all assets
            {
              test: [/\.(bmp|gif|jpe?g|png)$/],
              loader: 'url-loader',
              options: {
                limit: 10000,
                name: 'assets/images/[name]-[contenthash:8].[ext]',
              },
            },
            {
              test: [/\.(eot|ttf|woff2?)$/],
              loader: 'file-loader',
              options: {
                name: 'assets/fonts/[name].[ext]',
              },
            },
            // Fallback for all other files (config, fonts and so on)
            {
              loader: 'file-loader',
              exclude: [/\.(js|jsx)$/, /\.html$/, /\.json$/],
              options: {
                name: 'assets/[name]-[contenthash:8].[ext]',
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: true,
        template: paths.appHtml,
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeStyleLinkTypeAttributes: true,
          keepClosingSlash: true,
          minifyJS: true,
          minifyCSS: true,
          minifyURLs: true,
        },
      }),
      new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/runtime~.+[.]js/]),
      new ModuleNotFoundPlugin(paths.appPath),
      new webpack.DefinePlugin({
        env: JSON.stringify(env),
      }),
      new WebappWebpackPlugin({
        logo: './src/assets/images/logo.svg',
        prefix: 'assets/icons/',
      }),
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new WorkboxWebpackPlugin.GenerateSW({
        clientsClaim: true,
        exclude: [/\.map$/, /asset-manifest\.json$/],
        importWorkboxFrom: 'cdn',
        navigateFallback: '/index.html',
        navigateFallbackBlacklist: [
          new RegExp('^/_'),
          new RegExp('/[^/]+\\.[^/]+$'),
        ],
      }),
    ],
    node: {
      module: 'empty',
      dgram: 'empty',
      dns: 'mock',
      fs: 'empty',
      http2: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
    performance: {
      hints: 'warning',
      maxAssetSize: 512 * 1024,
      maxEntrypointSize: 1024 * 1024,
    },
  };
};

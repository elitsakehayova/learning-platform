process.env.BABEL_ENV = 'test';
process.env.NODE_ENV = 'test';
process.env.PUBLIC_URL = '';

const jest = require('jest');
let argv = process.argv.slice(2);

argv.push('--watchAll');

jest.run(argv);

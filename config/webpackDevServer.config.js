const paths = require('./paths');

module.exports = allowedHost => ({
  compress: true,
  clientLogLevel: 'none',
  contentBase: paths.appPublic,
  watchContentBase: true,
  hot: true,
  publicPath: '/',
  quiet: true,
  overlay: false,
  historyApiFallback: {
    disableDotRule: true,
  },
  public: allowedHost,
  // proxy: {
  //   '/api': {
  //     target: envConfig.qa.urls.hr,
  //     changeOrigin: true,
  //   },
  // },
});
